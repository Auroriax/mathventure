///scr_drawcredits(x,y,font1,text1,font2,text2,font3,text3,font4,text4,font5,text5,etc)
//Returns how long the credits text is.

var sproffset = 100;

var yoff = argument[1];

var w = view_wview - 300

for(var i = 2; i < argument_count; i += 1)
{
    
    draw_text_outline(argument[0],yoff,argument[i],c_black,w)
    draw_text_ext(argument[0],yoff,argument[i],-1,w)
    
    yoff += string_height_ext(argument[i],-1,w) + 20
}
