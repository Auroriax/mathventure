//message_position((window_get_x()+288-62),(window_get_y()+384+30))
show_message("These exclamation-marked tools are trap tiles.")
show_message("When you step on it and step off it, it will become an impassible wall.")
show_message("These are often smartly placed, so watch out what you're doing when stepping on and off them.")
