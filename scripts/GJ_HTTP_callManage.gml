// FRAMEWORK SCRIPT. DO NOT MODIFY.

var i, n, ithRequest, state, buffer, new, kill, filesize, progress, statusID, dataStructure
messageType, replace, nth, type, count, l, idList, orderList, newList, tAtPos, tIDatPos, indx;
if (!ds_list_empty(_GJ_HTTP_list))
{
    i = 0;
    while (i < ds_list_size(_GJ_HTTP_list))
    {
        messageType = ds_list_find_value(_GJ_HTTP_list, i)
        ithRequest = ds_list_find_value(_GJ_HTTP_list, i + 1);
        statusID = ds_list_find_value(_GJ_HTTP_list, i + 2);
        count = 3;
        
        httprequest_update(ithRequest);
        state = httprequest_get_state(ithRequest);
        kill = true;
        if (ds_list_find_value(_GJ_statusList, statusID) != _GJSTATUS_FREED)
        {
            switch (messageType)
            {
                // DATA STORAGE SAVING
                case 11:
                    count = 6;
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                        } else {
                            dat = ds_list_find_value(_GJ_HTTP_list, i + 5);
                            if (ds_list_find_value(_GJ_HTTP_list, i + 3) == 1)
                            {
                                dataStructure = _GJ_globalDataStorage;
                            } else {
                                dataStructure = _GJ_dataStorage;
                            }
                            key = ds_list_find_value(_GJ_HTTP_list, i + 4);
                            if (ds_map_exists(dataStructure, key))
                            {
                                ds_map_replace(dataStructure, key, dat);
                            } else {
                                ds_map_add(dataStructure, key, dat);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                
                // DATA STORAGE EDITING/FETCHING
                case 10:
                case 12:
                    count = 5;
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                        } else {
                            dat = string_copy(dat, 23, string_length(dat) - 25);
                            if (string_check_real(dat)) dat = real(dat);
                            if (ds_list_find_value(_GJ_HTTP_list, i + 3) == 1)
                            {
                                dataStructure = _GJ_globalDataStorage;
                            } else {
                                dataStructure = _GJ_dataStorage;
                            }
                            key = ds_list_find_value(_GJ_HTTP_list, i + 4);
                            if (ds_map_exists(dataStructure, key))
                            {
                                ds_map_replace(dataStructure, key, dat);
                            } else {
                                ds_map_add(dataStructure, key, dat);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                        
                
                // SCORE TABLE DOWNLOADS
                case 7:
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                        } else {
                            dataStructure = ds_list_create();
                            GJ_fill_map_ext(dataStructure, dat);
                            for (n = 0; n < ds_list_size(dataStructure); n+= 1)
                            {
                                nth = ds_list_find_value(dataStructure, n);
                                type = (string_length(ds_map_find_value(nth, "user")) > 0);
                                if (type == 0)
                                {
                                    ds_map_add(nth, "name", ds_map_find_value(nth, "guest"));
                                    ds_map_delete(nth, "guest");
                                } else {
                                    ds_map_add(nth, "name", ds_map_find_value(nth, "user"));
                                    ds_map_delete(nth, "user");
                                }
                                ds_map_add(nth, "scorer_type", type);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                            ds_map_add(_GJ_structMap, statusID, dataStructure);
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                
                // USER DATA DOWNLOADS
                case 0:
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1) {
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                        } else {
                            dataStructure = ds_map_create();
                            GJ_fill_map(dataStructure, dat);
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                            ds_map_add(_GJ_structMap, statusID, dataStructure);
                            if (_GJ_fetchImages)
                            {
                                GJ_HTTP_callNew(100, "", false, statusID);
                            }
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                
                // TROPHY DATA DOWNLOADS
                case 5:
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                        } else {
                            dataStructure = ds_map_create();
                            GJ_fill_map(dataStructure, dat);
                            ds_map_add(dataStructure, "has_achieved", (ds_map_find_value(dataStructure, "achieved") != "false"));
                            switch (ds_map_find_value(dataStructure, "difficulty"))
                            {
                                case "Bronze":   replace = 0; break;
                                case "Silver":   replace = 1; break;
                                case "Gold":     replace = 2; break;
                                case "Platinum": replace = 3; break;
                            }
                            ds_map_replace(dataStructure, "difficulty", replace);
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                            ds_map_add(_GJ_structMap, statusID, dataStructure);
                            ds_list_add(_GJ_trophyList, statusID);
                            if (_GJ_fetchImages)
                            {
                                GJ_HTTP_callNew(101, "", false, statusID);
                            }
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                
                // TROPHY-SET DOWNLOADS
                case 15:
                    count = 4;
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                        } else {
                            dataStructure = ds_list_create();
                            GJ_fill_set(dataStructure, dat, 0);
                            
                            orderList = ds_list_find_value(_GJ_HTTP_list, i + 3);
                            if (orderList != noone)
                            {
                                newList = ds_priority_create();
                                
                                a = 0;
                                repeat (ds_list_size(dataStructure))
                                {
                                    tAtPos = ds_list_find_value(dataStructure, a);
                                    indx = ds_list_find_index(orderList, GJ_getTrophyData(tAtPos, "id"));
                                    if (indx != -1)
                                    {
                                        ds_priority_add(newList, tAtPos, indx);
                                        ds_list_delete(dataStructure, a);
                                    } else {
                                        a+= 1;
                                    }
                                }
                                
                                while (!ds_priority_empty(newList))
                                {
                                    ds_list_insert(dataStructure, 0, ds_priority_delete_max(newList));
                                }
                                ds_priority_destroy(newList);
                            }
                            
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                            ds_list_add(_GJ_trophySetList, dataStructure);
                            ds_map_add(_GJ_structMap, statusID, dataStructure);
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                
                // USER-SET DOWNLOADS
                case 16:
                    count = 4; 
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                        } else {
                            dataStructure = ds_list_create();
                            GJ_fill_set(dataStructure, dat, 1);
                            
                            orderList = ds_list_find_value(_GJ_HTTP_list, i + 3);
                            if (orderList != noone)
                            {
                                newList = ds_priority_create();
                                
                                a = 0;
                                repeat (ds_list_size(dataStructure))
                                {
                                    tAtPos = ds_list_find_value(dataStructure, a);
                                    indx = ds_list_find_index(orderList, GJ_getUserData(tAtPos, "id"));
                                    if (indx != -1)
                                    {
                                        ds_priority_add(newList, tAtPos, indx);
                                        ds_list_delete(dataStructure, a);
                                    } else {
                                        a+= 1;
                                    }
                                }
                                
                                while (!ds_priority_empty(newList))
                                {
                                    ds_list_insert(dataStructure, 0, ds_priority_delete_max(newList));
                                }
                                ds_priority_destroy(newList);
                            }
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                            ds_map_add(_GJ_structMap, statusID, dataStructure);
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
            
            
                // IMAGE DOWNLOADS
                case 100:
                case 101:
                    if (state == httprequest_state_closed)
                    {
                        if (GJ_getStatus(statusID) > 0)
                        {
                            dataStructure = ds_map_find_value(_GJ_structMap, statusID);
                            var newFile, fileType;
                            fileType = httprequest_find_response_header(ithRequest, "Content-Type");
                            if (string_length(fileType))
                            {
                                fileType = string_delete(fileType, 1, 6);
                                newFile = temp_directory + "\temp." + fileType;
                                buffer = buffer_create();
                                httprequest_get_message_body_buffer(ithRequest, buffer);
                                buffer_write_to_file(buffer, newFile);
                                buffer_destroy(buffer);
                                new = sprite_add(newFile, 0, false, false, 0, 0);
                                if (messageType == 100)
                                {
                                    ds_map_add(dataStructure, "avatar_sprite", new);
                                } else {
                                    ds_map_add(dataStructure, "trophy_sprite", new);
                                }
                                file_delete(newFile);
                            }
                        }
                    } else if (state == httprequest_state_error) {
                        // An error ocurred
                    } else {
                        kill = false;
                    }
                    break;
                
                // BASIC REQUESTS THAT ONLY RETURN TRUE/FALSE
                default:
                    if (state == httprequest_state_closed)
                    {
                        buffer = buffer_create();
                        httprequest_get_message_body_buffer(ithRequest, buffer);
                        dat = buffer_to_string(buffer);
                        buffer_destroy(buffer);
                        if (string_pos('success:"false"', dat) == 1)
                        {
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                            if (_GJ_showErrors == true)
                            {
                                show_error(string_copy(dat, 27, string_length(dat) - 29), false);
                            }
                        } else {
                            ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_SUCCESS);
                        }
                    } else if (state == httprequest_state_error) {
                        ds_list_replace(_GJ_statusList, statusID, _GJSTATUS_ERROR);
                    } else {
                        kill = false;
                    }
                    break;
                
            }
        }
        
        if (kill)
        {
            if (ds_list_find_value(_GJ_HTTP_list, i) == 15 || ds_list_find_value(_GJ_HTTP_list, i) == 16)
            {
                l = ds_list_find_value(_GJ_HTTP_list, i + 3);
                if (l != noone) ds_list_destroy(l);
            }
            repeat (count)
            {
                ds_list_delete(_GJ_HTTP_list, i);
            }
            httprequest_destroy(ithRequest);
        } else {
            i+= count;
        }
    }
}
