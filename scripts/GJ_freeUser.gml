var map, spr, stt;
stt = GJ_getStatus(argument0);
if (stt != _GJSTATUS_FREED)
{
    if (stt == 1)
    {
        map = ds_map_find_value(_GJ_structMap, argument0);
        if (ds_map_exists(map, "avatar_sprite"))
        {
            spr = ds_map_find_value(map, "avatar_sprite");
            if (sprite_exists(spr))
            {
                sprite_delete(spr);
            }
        }
        ds_map_destroy(map);
    }
    ds_list_replace(_GJ_statusList, argument0, _GJSTATUS_FREED);
}
