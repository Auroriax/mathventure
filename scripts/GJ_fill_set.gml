// FRAMEWORK SCRIPT. DO NOT MODIFY.

var p, cur, map, str, state, val, first, myID, replace; myID = -1; str = ""; state = 0; first = ""; map = noone;
for (p=17; p<string_length(argument1); p+=1)
{
    cur = string_char_at(argument1, p);
    switch (true)
    {
        case (cur == chr(58) && state < 2):
            if (state == 0) first = str;
            if (first == str)
            {
                if (myID > -1)
                {
                    myID = ds_list_size(_GJ_statusList);
                    ds_list_add(_GJ_statusList, _GJSTATUS_SUCCESS);
                    ds_map_add(_GJ_structMap, myID, map);
                    ds_list_add(argument0, myID);
                    if (!argument2) ds_list_add(_GJ_trophyList, myID);
                    if (_GJ_fetchImages)
                    {
                        GJ_HTTP_callNew(101 - argument2, "", false, myID);
                    }
                }
                map = ds_map_create(); myID = 0;
            }
            key = str; str = ""; p+= 1; state = 2;
            break;
            
        case (cur == chr(13)):
            val = string_copy(str, 1, string_length(str) - 1);
            if (key == "achieved")
            {
                ds_map_add(map, "has_achieved", (val != "false"));
            } else if (key == "difficulty") {
                switch (val)
                {
                    case "Bronze":      val = 0; break;
                    case "Silver":      val = 1; break;
                    case "Gold":        val = 2; break;
                    case "Platinum":    val = 3; break;
                }
            } else if (string_check_real(val)) {
                val = real(val);
            }
            
            ds_map_add(map, key, val); str = ""; state = 1; p+= 1;
            break;
            
        default:
            str+= cur;
            break;
    }
}

if (myID > -1)
{
    myID = ds_list_size(_GJ_statusList);
    ds_list_add(_GJ_statusList, _GJSTATUS_SUCCESS);
    ds_map_add(_GJ_structMap, myID, map);
    ds_list_add(argument0, myID);
    if (!argument2) ds_list_add(_GJ_trophyList, myID);
    if (_GJ_fetchImages)
    {
        GJ_HTTP_callNew(101 - argument2, "", false, myID);
    }
}
