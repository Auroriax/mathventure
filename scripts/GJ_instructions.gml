/*************************************************************************
**                   Game Jolt API for Game Maker 8.0                   **
**************************************************************************
**    - Last Update: 13 Nov, 2012
**    - Author contact: thatbrod@gmail.com
**    - http://gamejolt.com/developers/achievements-new/
**************************************************************************
**
**  This is an instruction set for how to use the GJ API.
**  Do not run this script, you'll just get a ton of errors.
**
**  If you fully understand how to use the GJ scripts, you may delete this
**  script; It serves no purpose other than to help you out.
**
**  IMPORTANT NOTE:
**      You shouldn't edit or remove any of the scripts to begin with, but
**  pay SPECIAL attention to scripts tagged with:
**      // FRAMEWORK SCRIPT. DO NOT MODIFY.
**
**  NOTES:
**    - To quickly find a script's instructions, click on the magnifying
**  glass that's right by the script name's input field and search for a
**  name!
**    - Before this script will work, you must install the Http Dll 2.3
**  extension that is included in the zip you found this example in.
*************************************************************************/

    GJ_init();
/*  Usage:      GJ_init()
**  Returns:  - Nothing
**************************************************************************
** THIS IS THE MOST IMPORTANT SCRIPT. Make sure this script is run as
** early as possible, and don't try to call any other API scripts before
** this one is run!
**
** THERE ARE ALSO IMPORTANT VARIABLES YOU NEED TO MODIFY IN THIS SCRIPT.
** Make sure you look inside and change what's neccesary!
*/

    GJ_getStatus();
/*  Usage:      GJ_getStatus(handleID)
**  Returns:
**          _GJSTATUS_SUCCESS   This means that whatever download was
**                              associated to this status id has finished
**                              with no problems
**          _GJSTATUS_LOADING   This means that the download associated to
**                              this status id is still working
**          _GJSTATUS_ERROR     This means that there was some error,
**                              with the download or with the API's result
**          _GJSTATUS_FREED     This means you have freed this download 
**                              from memory.
**************************************************************************
** This is the SECOND MOST IMPORTANT SCRIPT. You use this with ALL
** downloads.
**
** handleID is an ID number that is returned from just about
** every script that requests information from the API. Make sure you get
** very familiar with the constants this script returns.
**
** Before ever using a handleID to get downloaded data, make sure that the
** download has finished using this script!
*/

/**************
** USER DATA **
**************/
    GJ_login();
/*  Usage:      GJ_login(<string> username, <string> token)
**  Returns:  - <bool> success
**************************************************************************
** If a user isn't already logged in, this will take the given username
** and token  and attempt to log them in. If successful, all other GJ 
** scripts that require logins will function properly.
**
** NOTE 1: When GJ_init is run, it will automatically log a user in if
** their details are available. If you don't want that to happen, set the
** appropriate variable to false in the GJ_init() script.
**
** NOTE 2: When this script is successful, the user's details will be
** stored into a ds_map, stored globally as _GJ_curUser. Use the script 
** GJ_getUserData() with _GJ_curUser as the userMap to get the logged-in
** user's details. When the user could not be logged in for whatever 
** reason, or you just haven't called the login script yet, _GJ_curUser is
** set to the special value: noone
*/

    GJ_logout();
/*  Usage:      GJ_logout(<bool> keepData)
**  Returns:  - Nothing
**************************************************************************
** Logs a user off if one is logged in. Will also free any data associated
** with the user that may have been downloaded if keepData is kept as
** false.
*/

    GJ_isLogged();
/*  Usage:      GJ_isLogged()
**  Returns:  - <bool> whether or not user is logged in
**************************************************************************
** Use this to check if a user is logged into your game.
*/

    GJ_setIdle();
/*  Usage:      GJ_setIdle(<bool> Idle)
**  Returns:  - Nothing
**************************************************************************
** Sets the state to idle or active. This is used for online sessions and
** will be seen on the profile page of the logged in user online.
*/

    GJ_getUser();
/*  Usage:      GJ_getUser(<string> username OR <real> userID)
**  Returns:  - handleID
**************************************************************************
** Fetches the data of the requested user. Both a username and user ID can
** be used. The returned handleID can be used with GJ_getUserData.
*/

    GJ_getUserSet();
/*  Usage:      GJ_getUserSet(<string> idSet)
**  Returns:  - handleID
**************************************************************************
** Fills in a ds_list with a set of user handle id's
** The user set will always be ordered in the same way the fed in idSet is
*/

    GJ_getUserSetList();
/*  Usage:      GJ_getUserSetList(handleID)
**  Returns:  - ds_list index if available
**                  OR
**            - noone (-4) if not available
**************************************************************************
** When a user set is finally downloaded, its contents will all be stored
** into a single ds_list structure, each index containing a handleID which
** will act like any other user's handleID.
** You get this ds_list using this function.
*/

    GJ_getUserData();
/*  Usage:      GJ_getUserData(handleID, <string> dataName, <value> default)
**  Returns:  - The data requested from the given userMap
**                  OR
**            - The given default value upon failure
**************************************************************************
** The dataName's that can be called are all strings, and have the
** following names and formats:
**
**      id                      The ID of the user.
**
**      type                    Can be "User", "Developer", "Moderator",
**                              or "Administrator".
**
**      username                The user's username.
**
**      avatar_url              The URL of the user's avatar.
**
**      signed_up               How long ago the user signed up.
**
**      last_logged_in          How long ago the user was last logged in.
**                              Will be "Online Now" if the user is
**                              currently online.
**
**      status                  "Active" if the user is still a member on
**                              the site. "Banned" if they've been banned.
**
** The names below only exist if the user is a developer
**
**      developer_name          The developer's name.
**
**      developer_website       The developer's website, if they put one
**                              in.
**
**      developer_description   The description that the developer put in
**                              for themselves. HTML tags and new lines 
**                              have been removed.
**
** If GJ_fetchImages is set to true, a usable sprite will (eventually) be
** loaded into the following name.
**
**      avatar_sprite           The downloaded avatar_url as a usable sprite.
**
** To avoid errors, ALWAYS check if this sprite exists before drawing it!
** Example:
**      spr = GJ_getUserData(userMap, "avatar_sprite");
**      if (sprite_exists(spr))
**      {
**          draw_sprite(spr, 0, x, y);
**      }
*/

    GJ_freeUser();
/*  Usage       GJ_freeUser(handleID)
**  Returns:  - Nothing
**************************************************************************
** Frees all the memory associated with the given user's handleID. Useful
** for reloading trophies after a user logs out. If you don't free the
** trophies after you're done with them and load in too many, you'll end
** up making a memory leak and getting lag or other such serious problems.
*/

    GJ_freeUserSet();
/*  Usage       GJ_freeTrophySet(handleID)
**  Returns:  - Nothing
**************************************************************************
** Frees all the memory associated with the given user set's handleID.
*/


/*************
** TROPHIES **
*************/
    GJ_addTrophy();
/*  Usage:      GJ_addTrophy(<real> trophyID)
**  Returns:  - handleID
**************************************************************************
** This adds the given trophy to the logged-in user's achieved trophy
** list.
** You can find the IDs of your game's trophies by visiting:
**
**      Your Dashboard ==> Manage Your Games >> [Your Game] ==> Manage Achievements
**
** When handleID is used as the argument in GJ_getStatus(), it will return
** _GJSTATUS_SUCCESS when the trophy has been added and _GJSTATUS_ERROR if
** there was a problem.
*/

    GJ_getTrophy();
/*  Usage:      GJ_getTrophy(<real> trophyID)
**  Returns:  - handleID
**************************************************************************
** Fetches the trophy data with the given ID. To find trophy ID's, visit:
**
**      Your Dashboard ==> Manage Your Games >> [Your Game] ==> Manage Achievements
**
** Use the returned trophyMap with GJ_getTrophyData();
*/

    GJ_getTrophySet();
/*  Usage:      GJ_getTrophySet(<real> setType, <string> idSet)
**  Returns:  - handleID
**************************************************************************
** Fetches a set of trophies based on the setType. The setType can be:
**
**      0   (All Trophies)      Will fetch all trophies
**
**      1   (Achieved trophies) Will only fetch trophies that the
**                              currently logged in user has achieved
**
**      2   (Missing trophies)  Will only fetch trophies that the
**                              currently logged in user has NOT achieved
**
**      3   (Specific set)      Will fetch the trophies described in the
**                              string idSet, where idSet is a string of
**                              trophy id's separated by commas.
**
** Note, the returned trophy set will always be ordered in the same way
** the fed in idSet is, even if you load your set with a different
** setType!
*/

    GJ_getTrophySetList();
/*  Usage:      GJ_getTrophySetList(handleID)
**  Returns:  - ds_list index if available
**                  OR
**            - noone (-4) if not available
**************************************************************************
** When a trophy set is finally downloaded, its contents will all be
** stored into a single ds_list structure, each index containing a
** handleID which will act like any other trophy's handleID.
** You get this ds_list using this function.
*/

    GJ_getTrophyData();
/*  Usage:      GJ_getTrophyData(handleID, <string> dataName, <value> default)
**  Returns:  - The data requested from the trophy associated with the
**              given handleID
**                  OR
**            - The given default value if it doesn't exist or if the
**              trophy hasn't finished loading.
**************************************************************************
** The dataName's that can be called are all strings, and have the
** following names and formats:
**
**      id                      The ID of the trophy.
**
**      title                   The title of the trophy on the site.
**
**      description             The trophy description text.
**
**      difficulty              0 = Bronze, 1 = Silver, 2 = Gold,
**                              3 = Platinum
**
**      image_url               The URL to the trophy's thumbnail.
**
**      has_achieved            A boolean value, true if user has achieved
**                              this trophy, false if not.
**
**      achieved                Returns how long ago the trophy was
**                              achieved by the user, or "false" if they
**                              haven't achieved it yet.
*/

    GJ_freeTrophy();
/*  Usage       GJ_freeTrophy(handleID)
**  Returns:  - Nothing
**************************************************************************
** Frees all the memory associated with the given trophy's handleID.
** Useful for reloading trophies after a user logs out. If you don't free
** the trophies after you're done with them and load in too many, you'll
** end up making a memory leak and getting lag or other such serious
** problems.
*/

    GJ_freeTrophySet();
/*  Usage       GJ_freeTrophySet(handleID)
**  Returns:  - Nothing
**************************************************************************
** Frees all the memory associated with the given trophy set's handleID.
*/


/******************************
** HIGH SCORES/LEADER BOARDS **
******************************/
    GJ_addScore();
/*  Usage:      GJ_addScore(<real> tableID, <string> scoreDisplay, <real> scoreSort, <string> extraData)
**  Returns   - handleID
**************************************************************************
** tableID is a unique identifier for specific highscore tables. If you
** only have one or wish to add the score to your primary table, set
** tableID to 0. Otherwise, you can find the IDs of your tables by
** visiting the following:
**
**      Your Dashboard ==> Manage Your Games >> [Your Game] ==> Manage Achievements >> High Scores
**
** The difference between scoreDisplay and scoreSort is very important.
** scoreDisplay is a string value associated with the score that is shown
** instead of a raw numeric value.
** Example: "234 Jumps".
** scoreSort is the value that actually determines the position in the
** high score list, and thus it must be a real value.
** extraData will not be seen by anything other than the GJ API. You may
** do whatever you may wish with it.
*/

    GJ_addGuestScore();
/*  Usage:      GJ_addGuestScore(<real> tableID, <string> guestName, <string> scoreDisplay, <real> scoreSort, <string> extraData)
**  Returns   - handleID
**************************************************************************
** This allows you to upload the scores of players who either haven't
** logged in or do not own a GJ account. See GJ_addScore for more details
** on the arguments.
*/

    GJ_getUserScores();
/*  Usage       GJ_getUserScores(<real> tableID, <real> limit)
**  Returns   - handleID
**************************************************************************
** This loads scores that were achieved by the logged-in user
** specifically.
** tableID is 0 when you wish to load primary/only have one table, or a
** specific tableID which can be found by visiting the following:
**
**      Your Dashboard ==> Manage Your Games >> [Your Game] ==> Manage Achievements >> High Scores
**
** limit is how many scores you wish to fetch. Maximum limit is 100!
** Use the returned handleID with GJ_get_nScoreData()
**
** NOTE: Before reloading scores, make sure you clear the previous table
** by using GJ_freeTableList() !!!
*/

    GJ_getGlobalScores();
/*  Usage       GJ_getGlobalScores(<real> tableID, <real> limit)
**  Returns   - handleID
**************************************************************************
** This loads scores achieved by any and all users.
** tableID is 0 when you wish to load primary/only have one table, or a
** specific tableID which can be found by visiting the following:
**
**      Your Dashboard ==> Manage Your Games >> [Your Game] ==> Manage Achievements >> High Scores
**
** limit is how many scores you wish to fetch. Maximum limit is 100!!
** Use the returned handleID with GJ_get_nScoreData()
**
** NOTE: Before reloading scores, make sure you clear the previous table
** by using GJ_freeTableList() !!!
*/

    GJ_get_nScoreData();
/*  Usage       GJ_get_nScoreData(handleID, <real> n, <string> dataName, <value> default)
**  Returns   - the data value stored at n position of the table
**              associated with the given handleID
**                  OR
**            - default value if there was an error
**************************************************************************
** the nth position works as follows:
**      (0 = Best score, 1 = 2nd best, 2 = 3rd best, ..., (limit - 1) = limith best)
**
** The dataName's that can be called are all strings, and have the
** following names and formats:
**
**      score                   The score string.
**
**      sort                    The score's numerical sort value.
**
**      extra_data              Any extra data associated with the score.
**
**      name                    The name of the scorer.
**
**      user_id                 If this is a user score, this is the user's ID.
**
**      scorer_type             If this is a guest score, 0. If a user score, 1.
**
**      stored                  Returns when the score was logged by the user.
*/

    GJ_freeScores();
/*  Usage       GJ_freeScores(handleID)
**  Returns:  - Nothing
**************************************************************************
** Frees all the memory associated with this table list. Very important 
** that you run this if you ever try to reload your scores to prevent any
** memory leaks!!
*/

/*****************
** DATA STORAGE **
******************
** Local and global data functions work with the same arguments. The
** difference is that local requires a user to be logged in, and will
** upload the requested key/value pairs to their own personal data hub.
**
** Global values can be accessed without being logged in, and can be
** fetched and changed by any instance of your game.
*************************************************************************/


/**********
*  Local  *
**********/
    GJ_getData();
/*  Usage       GJ_getData(<string> key, default)
**  Returns   - Value held at key
**                  OR
**            - default value upon failure
**************************************************************************
*/

    GJ_loadData();
/*  Usage       GJ_loadData(<string> key)
**  Returns:  - handleID
**************************************************************************
** Returns data from the Data Store.
**
** In order to actually access the downloaded key, you need to use the
** script GJ_getData()
*/

    GJ_saveData();
/*  Usage       GJ_saveData(<string> key, value)
**  Returns:  - handleID
**************************************************************************
** Sets data in the Data Store.
*/

    GJ_remData();
/*  Usage       GJ_remData(<string> key, value)
**  Returns:  - handleID
**************************************************************************
** Removes data from the Data Store.
*/

    GJ_editData();
/*  Usage       GJ_editData(<string> key, <string> operation, <real> value)
**  Returns:  - handleID
**************************************************************************
** Updates data in the Data Store.
**
** If you are attempting to edit a string, operation must be one of the
** following:
**
**      append          Given value is put at the end of whatever value is
**                      already online
**
**      prepend         Given value is put at the beginning of whatever
**                      value is already online
**
**  If you are attempting to edit a real, then operation must be one of
**  the following:
**
**      add             Online value has the given value added to it
**
**      subtract        Online value has the given value subtracted from
**                      it
**
**      multiply        Online value is multiplied by the given value
**
**      divide          Online value is divided by the given value
*/

    GJ_freeData();
/*  Usage       GJ_freeData()
**  Returns:  - Nothing
**************************************************************************
** Frees all of the downloaded data of the currently logged in user.
*/

/***********
*  Global  *
************
**  For instructions on global data storage functions, see their
**  corresponding local version.
**      Example: GJ_getGlobalData() has same instructions as
**      GJ_getData(), but works for global values instead of local ones
*************************************************************************/
    GJ_getGlobalData();
    GJ_loadGlobalData();
    GJ_saveGlobalData();
    GJ_remGlobalData();
    GJ_editGlobalData();
    GJ_freeGlobalData();
