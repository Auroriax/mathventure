var retVal, list;
retVal = argument3;
if (GJ_getStatus(argument0) > 0)
{
    list = ds_map_find_value(_GJ_structMap, argument0);
    if (argument1 >= 0 && argument1 < ds_list_size(list))
    {
        retVal = ds_list_find_value(list, argument1);
        if (ds_map_exists(retVal, string(argument2)))
        {
            retVal = ds_map_find_value(retVal, string(argument2));
        }
    }
}
return retVal;
