var retVal, map;
retVal = argument2;
if (GJ_getStatus(argument0) > 0)
{
    map = ds_map_find_value(_GJ_structMap, argument0);
    if (ds_map_exists(map, string(argument1)))
    {
        retVal = ds_map_find_value(map, string(argument1));
    } else {
        if (string(argument1) == "trophy_sprite")
        {
            retVal = -1;
        }
    }
}

return retVal;
