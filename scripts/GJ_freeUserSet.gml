var list, spr, i, stt, p;
stt = GJ_getStatus(argument0);
if (stt != _GJSTATUS_FREED)
{
    if (stt == 1)
    {
        list = ds_map_find_value(_GJ_structMap, argument0);
        for (i = 0; i < ds_list_size(list); i+= 1)
        {
            GJ_freeUser(ds_list_find_value(list, i));
        }
        ds_list_destroy(list);
        p = ds_list_find_index(_GJ_userSetList, argument0);
        if (p != -1) ds_list_delete(_GJ_userSetList, p);
    }
    ds_list_replace(_GJ_statusList, argument0, _GJSTATUS_FREED);
}
