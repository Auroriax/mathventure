var dat, retMap, fileUrl, fileType, char, i;
if (!GJ_isLogged())
{
    dat = GJ_HTTP_callNew(1, "username="+string(argument0)+"&user_token="+string(argument1), true, -1);
    if (string_pos('success:"false"', dat) == 1) {
        if (_GJ_showErrors == true)
            show_error(string_copy(dat, 27, string_length(dat) - 29), false); 
        return false;
    }
    
    dat = GJ_HTTP_callNew(0, "username="+string(argument0), true, -1);
    if (string_pos('success:"false"', dat) == 1) {
        if (_GJ_showErrors == true)
            show_error(string_copy(dat, 27, string_length(dat) - 29), false); 
        return false;
    }

    
    _GJ_curUser = ds_map_create();
    GJ_fill_map(_GJ_curUser, dat);
    ds_map_add(_GJ_curUser, "user_token", string(argument1));
    
    dat = ds_list_size(_GJ_statusList);
    ds_list_add(_GJ_statusList, _GJSTATUS_SUCCESS);
    ds_map_add(_GJ_structMap, dat, _GJ_curUser);
    if (_GJ_fetchImages)
    {
        GJ_HTTP_callNew(100, "", false, dat);
    }
    
    with (_GJ_control)
    {
        event_perform(ev_other, ev_user0);
    }
}

return true;
