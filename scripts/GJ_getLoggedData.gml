var retVal;

if (string(argument0) == "avatar_sprite")
{
    retVal = -1;
} else {
    retVal = argument[1];
}
if (GJ_isLogged())
{
    if (ds_map_exists(_GJ_curUser, string(argument0)))
    {
        retVal = ds_map_find_value(_GJ_curUser, string(argument0));
    }
}
return retVal;
