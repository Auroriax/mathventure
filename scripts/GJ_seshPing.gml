if (GJ_isLogged())
{
    var q, dat;
    q = "username="+string(GJ_getLoggedData("username"))+"&user_token="+string(GJ_getLoggedData("user_token"));
    if (_GJ_userIdle)
    {
        q+= "status=idle";
    } else {
        q+= "status=active";
    }
    dat = GJ_HTTP_callNew(3, q, true);
    if (string_pos('success:"false"', dat) == 1) {
        if (_GJ_showErrors == true)
            show_error(string_copy(dat, 27, string_length(dat) - 29), false); 
        return false;
    }
    return true;
}
return false;
