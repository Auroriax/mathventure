if (GJ_isLogged())
{
    var q, dat;
    q = "username="+string(GJ_getLoggedData("username"))+"&user_token="+string(GJ_getLoggedData("user_token"));
    dat = GJ_HTTP_callNew(2, q, true);
    if (string_pos('success:"false"', dat) == 1) {
        if (_GJ_showErrors == true)
            show_error(string_copy(dat, 27, string_length(dat) - 29), false); 
        return false;
    }
    return true;
}
return false;
