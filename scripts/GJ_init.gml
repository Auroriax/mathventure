globalvar _GJ_gameID, _GJ_pKey, _GJ_showErrors, _GJ_curUser, _GJ_control, _GJ_HTTP_list,
_GJ_fetchImages, _GJ_dataStorage, _GJ_statusList, _GJ_structMap, _GJ_globalDataStorage,
_GJSTATUS_SUCCESS, _GJSTATUS_LOADING, _GJSTATUS_ERROR, _GJSTATUS_FREED, _GJ_trophyList,
_GJ_trophySetList, _GJ_userIdle;
var _GJ_autoLogin, retVal;
_GJSTATUS_SUCCESS = 1; _GJSTATUS_LOADING = 0; _GJSTATUS_ERROR = -1; _GJSTATUS_FREED = -2;

retVal = false;
//GJ_init(gameID, privateKey, showErrors, autoLogin, fetchAvatars);

/********************************************************************************************
** Change _GJ_gameID to the ID of your game, and _GJ_pKey to the private key of your game. **
**                                                                                         **
** Both are found by found by going to http://www.gamejolt.com/ and visiting:              **
** Your Dashboard ==> Manage Your Games > [Your Game] ==> Manage Achievements              **
********************************************************************************************/
_GJ_gameID     = 15606;
_GJ_pKey       = "ae5251e169a9068aab0fa82d1f975465";

// Set showErrors to true if you want the API to notify you when there's been an error with
// one of the calls to GJ.
_GJ_showErrors = false;

// Set autoLogin to false if you don't want the API to automatically log the user in whenever
// possible.
_GJ_autoLogin = true;

// Set fetchImages to true to let the API download user avatars/trophy thumbnails and save them
// as sprites.
// When they are completely downloaded, the image will be saved into the associated data map,
// as "avatar_sprite" for avatars and "trophy_sprite" for trophies.
_GJ_fetchImages = true;


/*************************************************************
** DO NOT modify the code below, they build the framework!! **
*************************************************************/
_GJ_curUser       = noone; _GJ_userIdle = false;
_GJ_HTTP_list     = ds_list_create(); _GJ_trophyList = ds_list_create();
_GJ_dataStorage   = ds_map_create(); _GJ_globalDataStorage = ds_map_create();
_GJ_statusList    = ds_list_create(); _GJ_trophySetList = ds_list_create();
_GJ_structMap     = ds_map_create();
_GJ_control       = object_add();
object_set_persistent(_GJ_control, true); object_set_visible(_GJ_control, false);
object_event_add(_GJ_control, ev_other, ev_user0, "GJ_seshOpen(); alarm[0] = room_speed*25;");
object_event_add(_GJ_control, ev_other, ev_user1, "GJ_seshClose(); alarm[0] = -1;");
object_event_add(_GJ_control, ev_step, ev_step_normal, "GJ_HTTP_callManage();");
object_event_add(_GJ_control, ev_alarm, 0, "GJ_seshPing(); alarm[0] = room_speed*25;");
object_event_add(_GJ_control, ev_destroy, 0, "GJ_seshClose();");
object_event_add(_GJ_control, ev_other, ev_game_end, "GJ_logout();");
instance_create(0, 0, _GJ_control);

if (_GJ_autoLogin)
{
    if (GJ_autoLogin())
    {
        retVal = true;
    }
}

return retVal;
