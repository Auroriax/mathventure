var dat, a, q, set, str, strList, retVal;
set = noone;
retVal = -1;
q = "user_id="+string(argument0);

strList = string(argument0)+",";
a = string_pos(",", strList);
while (a)
{
    if (set == noone) set = ds_list_create();
    
    str = string_copy(strList, 1, a - 1);
    if (string_length(str))
    {
        if (string_check_real(str))
        {
            ds_list_add(set, real(str));
        }
    }
    strList = string_delete(strList, 1, a);
    a = string_pos(",", strList);
}

retVal = GJ_HTTP_callNew(16, q, false, set);
return retVal;
