// FRAMEWORK SCRIPT. DO NOT MODIFY.

var a, read, userName, userToken, retVal;
retVal = 0;

if (parameter_count() <= 1)
{
    if (file_exists("gjapi-credentials.txt"))
    {
        read = file_text_open_read("gjapi-credentials.txt");
        
        userName = file_text_read_string(read); file_text_readln(read);
        userToken = file_text_read_string(read); file_text_close(read);
        retVal = 3;
    }
} else {
    for (a=1; a<=parameter_count(); a+=1)
    {
        read = parameter_string(a);
        
        if (retVal mod 2 == 0)
        {
            if (string_pos("gjapi_username", read))
            {
                userName = string_delete(read, 1, 15); retVal+= 1;
            }
        }
        
        if (retVal div 2 == 0)
        {
            if (string_pos("gjapi_token", read))
            {
                userToken = string_delete(read, 1, 12); retVal+= 2;
            }
        }
        
        if (retVal == 3)
        {
            break;
        }
    }
}

if (retVal == 3)
{
    retVal = GJ_login(userName, userToken);
} else {
    retVal = false;
}

return retVal;
