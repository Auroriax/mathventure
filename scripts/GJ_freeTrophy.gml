var map, spr, stt, p;
stt = GJ_getStatus(argument0);
if (stt != _GJSTATUS_FREED)
{
    if (stt == 1)
    {
        map = ds_map_find_value(_GJ_structMap, argument0);
        if (ds_map_exists(map, "trophy_sprite"))
        {
            spr = ds_map_find_value(map, "trophy_sprite");
            if (sprite_exists(spr))
            {
                sprite_delete(spr);
            }
        }
        ds_map_destroy(map);
        p = ds_list_find_index(_GJ_trophyList, argument0);
        if (p > -1) ds_list_delete(_GJ_trophyList, p);
    }
    ds_list_replace(_GJ_statusList, argument0, _GJSTATUS_FREED);
}
