// FRAMEWORK SCRIPT. DO NOT MODIFY.

var url, httprequest, buffer, state, retVal, fileType, n;
retVal = -1;

if (argument0 < 17)
{
    url = "http://gamejolt.com/api/game/v1/";
    
    switch (argument0)
    {
        case 0:
        case 16:
            url+= "users/"; break;
        case 1:
            url+= "users/auth/"; break;
        case 2:
            url+= "sessions/open/"; break;
        case 3:
            url+= "sessions/ping/"; break;
        case 4:
            url+= "sessions/close/"; break;
        case 5:
        case 15:
            url+= "trophies/"; break;
        case 6:
            url+= "trophies/add-achieved/"; break;
        case 7:
            url+= "scores/"; break;
        case 8:
            url+= "scores/add/"; break;
        case 9:
            url+= "scores/tables/"; break;
        case 10:
            url+= "data-store/"; break;
        case 11:
            url+= "data-store/set/"; break;
        case 12:
            url+= "data-store/update/"; break;
        case 13:
            url+= "data-store/remove/"; break;
        case 14:
            url+= "data-store/get-keys/"; break;
    }
    
    url+= "?game_id=" + string(_GJ_gameID) + "&" + string(argument1);
    
    md5_begin(); md5_read_string(url + string(_GJ_pKey)); md5_end();
    
    url+= "&signature=" + md5_result();
} else {
    switch (argument0)
    {
        case 100:
            url = GJ_getUserData(argument3, "avatar_url"); break;
        case 101:
            url = GJ_getTrophyData(argument3, "image_url"); break;
    }
}

httprequest = httprequest_create();
httprequest_connect(httprequest, url, false);

if (argument2)
{
    httprequest_update(httprequest);
    state = httprequest_get_state(httprequest);
    while (state < 4)
    {
        httprequest_update(httprequest);
        state = httprequest_get_state(httprequest);
    }
    if (state == 4)
    {
        buffer = buffer_create();
        httprequest_get_message_body_buffer(httprequest, buffer);
        retVal = buffer_to_string(buffer);
        buffer_destroy(buffer);
    } else if (_GJ_showErrors) {
        show_error("There was some error connecting to the Game Jolt servers.##You may not be connected to the internet.", false);
    }
    httprequest_destroy(httprequest);
} else {
    retVal = ds_list_size(_GJ_statusList);
    ds_list_add(_GJ_HTTP_list, argument0);
    ds_list_add(_GJ_HTTP_list, httprequest);
    switch (argument0)
    {
        case 10:
        case 11:
        case 12:
        case 15:
        case 16:
            ds_list_add(_GJ_HTTP_list, retVal);
            ds_list_add(_GJ_HTTP_list, argument3);
            break;
        
        case 100:
        case 101:
            ds_list_add(_GJ_HTTP_list, argument3);
            break;
            
        default:
            ds_list_add(_GJ_HTTP_list, retVal);
            break;
    }
    ds_list_add(_GJ_statusList, _GJSTATUS_LOADING);
}

return retVal;
