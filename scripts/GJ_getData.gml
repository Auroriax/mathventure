var retVal;
retVal = argument1;

if (GJ_isLogged())
{
    if (ds_map_exists(_GJ_dataStorage, string(argument0)))
    {
        retVal = ds_map_find_value(_GJ_dataStorage, string(argument0));
    }
}
return retVal;
