var dat, a, q, set, str, strList, retVal;
set = noone;
retVal = -1;
if (GJ_isLogged())
{
    q = "username="+GJ_getLoggedData("username")+"&user_token="+GJ_getLoggedData("user_token");
    switch (argument0)
    {
        case 1:
            q+= "&achieved=true";
            break;
            
        case 2:
            q+= "&achieved=false";
            break;
            
        case 3:
            q+= "&trophy_id="+string(argument[1]);
            break;
    }
    
    strList = string(argument[1])+",";
    a = string_pos(",", strList);
    while (a)
    {
        if (set == noone) set = ds_list_create();
        
        str = string_copy(strList, 1, a - 1);
        if (string_length(str))
        {
            if (string_check_real(str))
            {
                ds_list_add(set, real(str));
            }
        }
        strList = string_delete(strList, 1, a);
        a = string_pos(",", strList);
    }
    
    retVal = GJ_HTTP_callNew(15, q, false, set);
}
return retVal;
