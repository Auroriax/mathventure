var i, cur;
if (GJ_isLogged())
{
    with (_GJ_control)
    {
        event_perform(ev_other, ev_user1);
    }
    
    if (ds_map_exists(_GJ_curUser, "avatar_sprite"))
    {
        spr = ds_map_find_value(_GJ_curUser, "avatar_sprite");
        if (sprite_exists(spr))
        {
            sprite_delete(spr);
        }
    }
    repeat (max(ds_list_size(_GJ_trophySetList), ds_list_size(_GJ_trophyList)))
    {
        if (!ds_list_empty(_GJ_trophySetList)) GJ_freeTrophySet(ds_list_find_value(_GJ_trophySetList, 0));
        if (!ds_list_empty(_GJ_trophyList)) GJ_freeTrophy(ds_list_find_value(_GJ_trophyList, 0));
    }
    ds_map_clear(_GJ_curUser); GJ_freeData();
    _GJ_curUser = noone;
}
