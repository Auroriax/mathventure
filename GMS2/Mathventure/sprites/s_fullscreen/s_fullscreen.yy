{
    "id": "aef14e87-e8a1-4bfe-8767-0ea273f5a95a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fullscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96953b33-d673-4d71-9fba-30a0c9c27a15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aef14e87-e8a1-4bfe-8767-0ea273f5a95a",
            "compositeImage": {
                "id": "224e6097-c525-40f7-810f-7979237511a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96953b33-d673-4d71-9fba-30a0c9c27a15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcdd63ed-997c-4ad5-857d-a220749ad4fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96953b33-d673-4d71-9fba-30a0c9c27a15",
                    "LayerId": "da074875-abf0-455c-b74f-ac540bc534ca"
                }
            ]
        },
        {
            "id": "027eafb2-33e0-4bb0-a417-167771c2a7a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aef14e87-e8a1-4bfe-8767-0ea273f5a95a",
            "compositeImage": {
                "id": "d67540df-2305-4484-a676-e7a315d57f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027eafb2-33e0-4bb0-a417-167771c2a7a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79826756-adb9-49fc-ae3a-386eb3af7191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027eafb2-33e0-4bb0-a417-167771c2a7a8",
                    "LayerId": "da074875-abf0-455c-b74f-ac540bc534ca"
                }
            ]
        },
        {
            "id": "45992554-d648-4cbc-9e22-720bd896b8c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aef14e87-e8a1-4bfe-8767-0ea273f5a95a",
            "compositeImage": {
                "id": "45e7641a-813a-4d62-9637-d2cd522e635b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45992554-d648-4cbc-9e22-720bd896b8c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a40dbe-8546-4674-a4a4-54c37f8546b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45992554-d648-4cbc-9e22-720bd896b8c3",
                    "LayerId": "da074875-abf0-455c-b74f-ac540bc534ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "da074875-abf0-455c-b74f-ac540bc534ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aef14e87-e8a1-4bfe-8767-0ea273f5a95a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}