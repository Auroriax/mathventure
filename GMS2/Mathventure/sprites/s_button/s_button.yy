{
    "id": "f0567a8a-0015-4102-95d9-177cf98ca391",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e15b6bf9-1ccd-4dd0-8593-1dcb0757e7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0567a8a-0015-4102-95d9-177cf98ca391",
            "compositeImage": {
                "id": "18e32f33-080b-4852-80b7-a37ced278276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15b6bf9-1ccd-4dd0-8593-1dcb0757e7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad588eb-569b-4351-95f5-ea7a2cd2a3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15b6bf9-1ccd-4dd0-8593-1dcb0757e7b4",
                    "LayerId": "7982f139-2c5d-4c17-9b7a-a1228b86731a"
                }
            ]
        },
        {
            "id": "5290431f-822e-4467-acf6-3ff4e7a88082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0567a8a-0015-4102-95d9-177cf98ca391",
            "compositeImage": {
                "id": "1eb3134e-6d1c-4cf7-b52c-f4b47e9aaaad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5290431f-822e-4467-acf6-3ff4e7a88082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "887f5a62-01a2-48bd-837a-d1891c08e1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5290431f-822e-4467-acf6-3ff4e7a88082",
                    "LayerId": "7982f139-2c5d-4c17-9b7a-a1228b86731a"
                }
            ]
        },
        {
            "id": "9a0db7c0-e73c-47ba-b805-739b5c0034b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0567a8a-0015-4102-95d9-177cf98ca391",
            "compositeImage": {
                "id": "535d27f7-c692-47aa-bb97-7a7288d6f361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0db7c0-e73c-47ba-b805-739b5c0034b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13f4c92a-4a2e-405f-bcb5-e451720c3a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0db7c0-e73c-47ba-b805-739b5c0034b8",
                    "LayerId": "7982f139-2c5d-4c17-9b7a-a1228b86731a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "7982f139-2c5d-4c17-9b7a-a1228b86731a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0567a8a-0015-4102-95d9-177cf98ca391",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}