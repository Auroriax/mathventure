{
    "id": "74f8882a-9147-4357-a633-523a3cd92c27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_moveup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78653ffd-72c1-4198-9f81-9514b7191fb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74f8882a-9147-4357-a633-523a3cd92c27",
            "compositeImage": {
                "id": "c18c243b-4667-4d68-9886-96c444d5b4f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78653ffd-72c1-4198-9f81-9514b7191fb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbd20aab-c11f-48b0-baf1-d2806b22c82c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78653ffd-72c1-4198-9f81-9514b7191fb5",
                    "LayerId": "a4731276-4e67-4715-bc40-1ea003221c5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a4731276-4e67-4715-bc40-1ea003221c5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74f8882a-9147-4357-a633-523a3cd92c27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}