{
    "id": "d181aa31-9d93-4358-9892-c0f1af9d4114",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_gjflag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2076593b-06b6-4789-baa6-40b4fbe557b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181aa31-9d93-4358-9892-c0f1af9d4114",
            "compositeImage": {
                "id": "a94eb283-34ce-4a14-b922-5c57cfb53286",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2076593b-06b6-4789-baa6-40b4fbe557b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5cfd59c-7f09-4d76-bc5c-57c1084004c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2076593b-06b6-4789-baa6-40b4fbe557b7",
                    "LayerId": "ad4db641-8b61-493b-b548-7841fe5e6e8b"
                }
            ]
        },
        {
            "id": "7b6ba5dd-d4f2-439c-bbf9-a04bd51080db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d181aa31-9d93-4358-9892-c0f1af9d4114",
            "compositeImage": {
                "id": "aac156eb-5765-4597-bb45-247f94c27fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b6ba5dd-d4f2-439c-bbf9-a04bd51080db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25343a93-0031-42ed-927e-7ef082e6750e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b6ba5dd-d4f2-439c-bbf9-a04bd51080db",
                    "LayerId": "ad4db641-8b61-493b-b548-7841fe5e6e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "ad4db641-8b61-493b-b548-7841fe5e6e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d181aa31-9d93-4358-9892-c0f1af9d4114",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}