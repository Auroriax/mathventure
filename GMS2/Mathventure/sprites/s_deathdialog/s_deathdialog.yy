{
    "id": "5118b5ea-a3df-4987-9d4d-a9466a5efa5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_deathdialog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 375,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1223e03-7aed-4a91-9015-51b8b526c7cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5118b5ea-a3df-4987-9d4d-a9466a5efa5c",
            "compositeImage": {
                "id": "82c66055-0298-4a43-a984-e071805f824d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1223e03-7aed-4a91-9015-51b8b526c7cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb231a3-4c2f-48a3-b7d8-4beb793ce041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1223e03-7aed-4a91-9015-51b8b526c7cd",
                    "LayerId": "0852183d-b848-4ce3-a434-73941757e7bb"
                }
            ]
        },
        {
            "id": "a9d8afc7-4859-4721-97bd-9365cee21651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5118b5ea-a3df-4987-9d4d-a9466a5efa5c",
            "compositeImage": {
                "id": "fe6836d9-644a-41f6-80b9-ac8b350851e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9d8afc7-4859-4721-97bd-9365cee21651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de1dfd06-f945-414d-b7f3-3c7af700f9ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9d8afc7-4859-4721-97bd-9365cee21651",
                    "LayerId": "0852183d-b848-4ce3-a434-73941757e7bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "0852183d-b848-4ce3-a434-73941757e7bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5118b5ea-a3df-4987-9d4d-a9466a5efa5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 376,
    "xorig": 0,
    "yorig": 21
}