{
    "id": "079ba903-5192-461f-bc6f-2955a9d782b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 187,
    "bbox_left": 0,
    "bbox_right": 188,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c74c897d-1d66-4bbe-aeaf-6c27c1b1e329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "079ba903-5192-461f-bc6f-2955a9d782b7",
            "compositeImage": {
                "id": "3e60cd0e-153f-47a8-abf3-6901a85015a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74c897d-1d66-4bbe-aeaf-6c27c1b1e329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159985dd-40d9-48d6-9396-725c274ed5bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74c897d-1d66-4bbe-aeaf-6c27c1b1e329",
                    "LayerId": "fae3415a-591c-4c7b-acc6-a03b152e78d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 196,
    "layers": [
        {
            "id": "fae3415a-591c-4c7b-acc6-a03b152e78d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "079ba903-5192-461f-bc6f-2955a9d782b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 189,
    "xorig": 0,
    "yorig": 0
}