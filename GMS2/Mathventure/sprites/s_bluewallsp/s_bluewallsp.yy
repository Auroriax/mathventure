{
    "id": "0c95cfe2-a3bd-4360-8b55-024a050340a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bluewallsp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7fe3e61-0642-4340-98e8-b828ffa81058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c95cfe2-a3bd-4360-8b55-024a050340a1",
            "compositeImage": {
                "id": "4f7ee528-4ede-4bb6-9ddf-e9426491e171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7fe3e61-0642-4340-98e8-b828ffa81058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5281b74a-7f8c-4579-943e-1facbaa27c90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7fe3e61-0642-4340-98e8-b828ffa81058",
                    "LayerId": "0bc656cc-e8a4-44fa-b9ae-80d023fbf183"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0bc656cc-e8a4-44fa-b9ae-80d023fbf183",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c95cfe2-a3bd-4360-8b55-024a050340a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}