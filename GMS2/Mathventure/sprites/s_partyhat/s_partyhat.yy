{
    "id": "cf07f5f1-8077-4218-b753-d497dddfe5bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_partyhat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 7,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a3aca25-0260-4318-8230-c5ae09af185b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf07f5f1-8077-4218-b753-d497dddfe5bf",
            "compositeImage": {
                "id": "bf43baa6-66d7-4538-97e8-1963f88a4da7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a3aca25-0260-4318-8230-c5ae09af185b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07b93375-70e7-4f1e-9bf4-c307466301f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a3aca25-0260-4318-8230-c5ae09af185b",
                    "LayerId": "3612881d-fc69-406c-95a8-c1896992dfcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3612881d-fc69-406c-95a8-c1896992dfcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf07f5f1-8077-4218-b753-d497dddfe5bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}