{
    "id": "c72f7092-41e0-4370-a020-e3c749c29d32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_moverright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec2ebffa-28d2-4dee-9c4b-1ea4a4414849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c72f7092-41e0-4370-a020-e3c749c29d32",
            "compositeImage": {
                "id": "68b43bb3-1897-41eb-b8c6-068bb12ed3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2ebffa-28d2-4dee-9c4b-1ea4a4414849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79a765d1-98ed-4ec4-86d1-2fbbd1e21a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2ebffa-28d2-4dee-9c4b-1ea4a4414849",
                    "LayerId": "eec396d2-81ff-4562-84df-b6c51f12dfe4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "eec396d2-81ff-4562-84df-b6c51f12dfe4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c72f7092-41e0-4370-a020-e3c749c29d32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}