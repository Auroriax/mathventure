{
    "id": "d39c2577-157e-4143-9de1-19787de39a07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_auroriaxLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 402,
    "bbox_left": 108,
    "bbox_right": 402,
    "bbox_top": 108,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44dbc4b0-381e-465c-80bb-0f537cf28250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39c2577-157e-4143-9de1-19787de39a07",
            "compositeImage": {
                "id": "99e6851e-906f-41a2-a7ec-35c1733bf59d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44dbc4b0-381e-465c-80bb-0f537cf28250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd06e11b-8737-47b2-b4db-93587dcd59b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44dbc4b0-381e-465c-80bb-0f537cf28250",
                    "LayerId": "5519736a-0cc7-4d14-89f2-f166dc0913b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "5519736a-0cc7-4d14-89f2-f166dc0913b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d39c2577-157e-4143-9de1-19787de39a07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}