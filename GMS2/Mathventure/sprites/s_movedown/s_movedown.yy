{
    "id": "12a6611c-b7c7-49b2-ada3-374d9b29b99a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_movedown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcdfd1ee-b752-4b9a-aa5b-1aff5d767807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a6611c-b7c7-49b2-ada3-374d9b29b99a",
            "compositeImage": {
                "id": "f669f846-4fcc-4926-a5a0-046c939eb225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcdfd1ee-b752-4b9a-aa5b-1aff5d767807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ed91ade-ef8a-4895-9b8b-9f40a87a9e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdfd1ee-b752-4b9a-aa5b-1aff5d767807",
                    "LayerId": "08938ef6-bb90-4891-b8aa-aaebd53d1db6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "08938ef6-bb90-4891-b8aa-aaebd53d1db6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12a6611c-b7c7-49b2-ada3-374d9b29b99a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}