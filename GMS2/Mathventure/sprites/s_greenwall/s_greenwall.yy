{
    "id": "5b9b50ee-3195-4ea6-8f29-37ab251ece60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_greenwall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e509570-4881-4c77-ad81-7dd72a694af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b9b50ee-3195-4ea6-8f29-37ab251ece60",
            "compositeImage": {
                "id": "3f286ed2-1035-4ee3-92f3-62b23de1bccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e509570-4881-4c77-ad81-7dd72a694af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7afd719-ef00-4635-a71c-3b0d1b1203ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e509570-4881-4c77-ad81-7dd72a694af3",
                    "LayerId": "c473a77b-9f95-45ae-bdd6-f89572c9b25b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c473a77b-9f95-45ae-bdd6-f89572c9b25b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b9b50ee-3195-4ea6-8f29-37ab251ece60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}