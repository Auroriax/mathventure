{
    "id": "3c2bbd11-a3b6-46b6-ab9d-ef228dd43200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 7,
    "bbox_right": 8,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99f48373-8ae0-4aba-87fd-d5c6fc9a17fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c2bbd11-a3b6-46b6-ab9d-ef228dd43200",
            "compositeImage": {
                "id": "0c4b2b72-b55d-453c-86bf-50aee52f119a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f48373-8ae0-4aba-87fd-d5c6fc9a17fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf60cc1f-99c9-498a-8e65-eaac7a6b4ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f48373-8ae0-4aba-87fd-d5c6fc9a17fa",
                    "LayerId": "29daf08a-ca18-426f-822d-ed19e0f18bb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "29daf08a-ca18-426f-822d-ed19e0f18bb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c2bbd11-a3b6-46b6-ab9d-ef228dd43200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}