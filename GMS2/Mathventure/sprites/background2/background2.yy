{
    "id": "da3192d2-5f30-4483-8a48-aded8aab5594",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19116c6a-d15f-4dcb-bc80-d5954fcf51e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da3192d2-5f30-4483-8a48-aded8aab5594",
            "compositeImage": {
                "id": "926fed3f-a35b-4e41-b4de-9028e62e8b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19116c6a-d15f-4dcb-bc80-d5954fcf51e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b15314f-2281-4441-a218-84e33ff75fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19116c6a-d15f-4dcb-bc80-d5954fcf51e6",
                    "LayerId": "9e19f016-fb8f-495d-a625-1bdaf011ce5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "9e19f016-fb8f-495d-a625-1bdaf011ce5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da3192d2-5f30-4483-8a48-aded8aab5594",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}