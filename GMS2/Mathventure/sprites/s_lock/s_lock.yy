{
    "id": "4b5a289c-d434-4a4f-af8c-e1f1a6bf6f60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 18,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f15d3d0c-adbb-4d11-b30e-c0b9fcaf78f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b5a289c-d434-4a4f-af8c-e1f1a6bf6f60",
            "compositeImage": {
                "id": "ccebeba7-3094-4bbc-b343-809d62633596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15d3d0c-adbb-4d11-b30e-c0b9fcaf78f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a57430f-befe-4093-8abe-536862b2dd87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15d3d0c-adbb-4d11-b30e-c0b9fcaf78f1",
                    "LayerId": "66ebadd9-eeef-4d3e-a311-765150a32f2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "66ebadd9-eeef-4d3e-a311-765150a32f2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b5a289c-d434-4a4f-af8c-e1f1a6bf6f60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}