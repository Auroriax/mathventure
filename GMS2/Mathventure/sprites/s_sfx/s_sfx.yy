{
    "id": "412446ab-1634-4500-98ba-ca19dc10f17f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sfx",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcf79aec-e656-4941-ad5b-de6028b85da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
            "compositeImage": {
                "id": "0571200b-259a-4352-9bdf-aab42b3cba3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf79aec-e656-4941-ad5b-de6028b85da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f55eea-823b-47e8-a843-91a1e53c57c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf79aec-e656-4941-ad5b-de6028b85da1",
                    "LayerId": "f4dcb587-1fa9-47ce-a070-11cbec45d4b2"
                }
            ]
        },
        {
            "id": "c003324e-7fe5-4a73-847d-6e9a9933faea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
            "compositeImage": {
                "id": "38959b90-cc26-41c3-93e7-e4628dbb694b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c003324e-7fe5-4a73-847d-6e9a9933faea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c0a825-a79e-40b4-9dbf-d5ca17cfb8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c003324e-7fe5-4a73-847d-6e9a9933faea",
                    "LayerId": "f4dcb587-1fa9-47ce-a070-11cbec45d4b2"
                }
            ]
        },
        {
            "id": "f55b5b88-414f-4792-a3ec-f3c2b481c5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
            "compositeImage": {
                "id": "b67f1650-44f0-4266-ab43-1b8ed7f2f9bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55b5b88-414f-4792-a3ec-f3c2b481c5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9c575c-8f7e-4f90-ace4-a11129dd8b10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55b5b88-414f-4792-a3ec-f3c2b481c5b5",
                    "LayerId": "f4dcb587-1fa9-47ce-a070-11cbec45d4b2"
                }
            ]
        },
        {
            "id": "8b65ce5c-7b14-46fe-8396-ea75e23c5fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
            "compositeImage": {
                "id": "2a177383-0dd4-429d-a37a-87975680263d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b65ce5c-7b14-46fe-8396-ea75e23c5fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8332215d-1cfa-4a01-ae4b-58a9544b69ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b65ce5c-7b14-46fe-8396-ea75e23c5fcf",
                    "LayerId": "f4dcb587-1fa9-47ce-a070-11cbec45d4b2"
                }
            ]
        },
        {
            "id": "7645054f-9fd2-45cc-af5a-97bae4f4d898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
            "compositeImage": {
                "id": "f2c655b2-3d81-4fab-9d63-487fba4cf2ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7645054f-9fd2-45cc-af5a-97bae4f4d898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84361d6-031c-40cb-a68f-5aa251f919fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7645054f-9fd2-45cc-af5a-97bae4f4d898",
                    "LayerId": "f4dcb587-1fa9-47ce-a070-11cbec45d4b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "f4dcb587-1fa9-47ce-a070-11cbec45d4b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}