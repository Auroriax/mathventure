{
    "id": "a38f5fed-de68-4329-ad51-8dd48f191603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_subtractrdempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5df1be55-ec5f-4e55-9897-f5a1b4ad89b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38f5fed-de68-4329-ad51-8dd48f191603",
            "compositeImage": {
                "id": "04136c03-b27c-48bb-8923-d0b8edd7252d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df1be55-ec5f-4e55-9897-f5a1b4ad89b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53fedce1-1c0b-417d-9cec-492854958d1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df1be55-ec5f-4e55-9897-f5a1b4ad89b7",
                    "LayerId": "5e67e38e-8d61-4e4d-af4f-40acff40683f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5e67e38e-8d61-4e4d-af4f-40acff40683f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a38f5fed-de68-4329-ad51-8dd48f191603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}