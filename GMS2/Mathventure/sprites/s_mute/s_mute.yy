{
    "id": "9ebc9a2a-2401-4e4a-b286-129dcc64647f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mute",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8257d52b-0d75-483d-af08-a4070d0ecf25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ebc9a2a-2401-4e4a-b286-129dcc64647f",
            "compositeImage": {
                "id": "eb030600-672e-43c6-95fa-1a9dc30b17cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8257d52b-0d75-483d-af08-a4070d0ecf25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966fb877-7990-42a3-8c12-011e4a10473e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8257d52b-0d75-483d-af08-a4070d0ecf25",
                    "LayerId": "80c10791-b08a-4d2b-960b-6da8bffc8547"
                }
            ]
        },
        {
            "id": "0f8ef219-153b-4fb9-a346-cc8e06f49914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ebc9a2a-2401-4e4a-b286-129dcc64647f",
            "compositeImage": {
                "id": "f2d2105b-6ddd-4499-980b-2a0a7712b8fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f8ef219-153b-4fb9-a346-cc8e06f49914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0534564-7592-4f6b-8acc-0aaeed6f59b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f8ef219-153b-4fb9-a346-cc8e06f49914",
                    "LayerId": "80c10791-b08a-4d2b-960b-6da8bffc8547"
                }
            ]
        },
        {
            "id": "bcbf3089-6799-4d05-9b79-736fe7a5b287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ebc9a2a-2401-4e4a-b286-129dcc64647f",
            "compositeImage": {
                "id": "d962a191-895f-4742-8a16-dd1bbfe12690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbf3089-6799-4d05-9b79-736fe7a5b287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80e08b53-b94c-494f-b101-acd6a54e282a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbf3089-6799-4d05-9b79-736fe7a5b287",
                    "LayerId": "80c10791-b08a-4d2b-960b-6da8bffc8547"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "80c10791-b08a-4d2b-960b-6da8bffc8547",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ebc9a2a-2401-4e4a-b286-129dcc64647f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}