{
    "id": "46303903-ad3a-4988-b345-9fd0a0af1632",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_addroundempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e5a65ca-2ab0-46f8-8c47-b706c0e310c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46303903-ad3a-4988-b345-9fd0a0af1632",
            "compositeImage": {
                "id": "bec45f67-9a0d-4edb-92d5-239ddc7c0592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5a65ca-2ab0-46f8-8c47-b706c0e310c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e14458-f34f-436e-860c-665e5c13b234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5a65ca-2ab0-46f8-8c47-b706c0e310c1",
                    "LayerId": "940104e9-36dd-47b4-90a2-bc8b75f14f69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "940104e9-36dd-47b4-90a2-bc8b75f14f69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46303903-ad3a-4988-b345-9fd0a0af1632",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}