{
    "id": "d89be49e-3120-4a21-89d1-9162e689d99a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_multiplerdempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "836455b9-df6f-4600-ba41-1f2fcadefecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d89be49e-3120-4a21-89d1-9162e689d99a",
            "compositeImage": {
                "id": "50cdaf4a-2ae0-42a6-8470-966c6c5258f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "836455b9-df6f-4600-ba41-1f2fcadefecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f2f58f3-9855-4238-9e67-1d6ae46d40c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "836455b9-df6f-4600-ba41-1f2fcadefecc",
                    "LayerId": "5d3114ec-5ce7-482e-9e5f-2eb106713607"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5d3114ec-5ce7-482e-9e5f-2eb106713607",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d89be49e-3120-4a21-89d1-9162e689d99a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}