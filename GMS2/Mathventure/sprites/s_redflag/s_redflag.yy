{
    "id": "b6228f8c-f116-422b-abed-8733f1efe8cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_redflag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "724f4c66-009f-454a-89b3-b0fb75c12eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6228f8c-f116-422b-abed-8733f1efe8cf",
            "compositeImage": {
                "id": "3823e76d-104f-4c09-a77b-56021b72ce41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "724f4c66-009f-454a-89b3-b0fb75c12eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "538c8bb5-52ef-4469-8fbf-6735bd6948f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "724f4c66-009f-454a-89b3-b0fb75c12eba",
                    "LayerId": "e320ad10-995b-479b-881f-d41dd5e28655"
                }
            ]
        },
        {
            "id": "37a99d54-efb1-4afa-b6b4-82bba7fdc1ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6228f8c-f116-422b-abed-8733f1efe8cf",
            "compositeImage": {
                "id": "b67e0a46-cbd7-492c-a110-0e6ccebb865d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a99d54-efb1-4afa-b6b4-82bba7fdc1ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f71a5f6-03db-4c51-82d4-9e7d5f0e8c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a99d54-efb1-4afa-b6b4-82bba7fdc1ec",
                    "LayerId": "e320ad10-995b-479b-881f-d41dd5e28655"
                }
            ]
        },
        {
            "id": "d9da2e75-3395-4bfe-bd69-1a146414a878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6228f8c-f116-422b-abed-8733f1efe8cf",
            "compositeImage": {
                "id": "efeda699-ee53-4d7a-91bc-fcc33b865383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9da2e75-3395-4bfe-bd69-1a146414a878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "153d27b2-888f-4de1-9718-4620d0b39593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9da2e75-3395-4bfe-bd69-1a146414a878",
                    "LayerId": "e320ad10-995b-479b-881f-d41dd5e28655"
                }
            ]
        },
        {
            "id": "d39629ef-0cf5-47e9-bdc7-a5e5a22c7457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6228f8c-f116-422b-abed-8733f1efe8cf",
            "compositeImage": {
                "id": "21ab7fa3-86c4-4841-a959-839d9e1f87c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39629ef-0cf5-47e9-bdc7-a5e5a22c7457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3399ea-d0aa-409f-a99f-64795eaa75e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39629ef-0cf5-47e9-bdc7-a5e5a22c7457",
                    "LayerId": "e320ad10-995b-479b-881f-d41dd5e28655"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "e320ad10-995b-479b-881f-d41dd5e28655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6228f8c-f116-422b-abed-8733f1efe8cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}