{
    "id": "3eecb3cf-362b-44a2-9e48-f6a16c88cbd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_levelclear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 23,
    "bbox_right": 478,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "566da431-2b01-43d7-85f8-bb681ed68073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eecb3cf-362b-44a2-9e48-f6a16c88cbd3",
            "compositeImage": {
                "id": "5b7bbbd9-0fb8-4b26-bf9a-50ca38be4608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "566da431-2b01-43d7-85f8-bb681ed68073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7032315f-a50b-460f-af88-cab2821f9d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "566da431-2b01-43d7-85f8-bb681ed68073",
                    "LayerId": "ec17edbb-d0fd-451e-8b5e-d20c8e7ad272"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "ec17edbb-d0fd-451e-8b5e-d20c8e7ad272",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eecb3cf-362b-44a2-9e48-f6a16c88cbd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 506,
    "xorig": 253,
    "yorig": 29
}