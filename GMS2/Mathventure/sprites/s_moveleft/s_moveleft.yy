{
    "id": "f1d7708b-3385-4d25-ac7a-4222ea522c8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_moveleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20d5f055-9d10-4bf1-b03f-29bc0ea6ae4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1d7708b-3385-4d25-ac7a-4222ea522c8a",
            "compositeImage": {
                "id": "47f5135b-1f89-4b81-bb51-d294c9506fa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d5f055-9d10-4bf1-b03f-29bc0ea6ae4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "037beb5e-9b88-48ab-9f7e-8024d983bb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d5f055-9d10-4bf1-b03f-29bc0ea6ae4c",
                    "LayerId": "3a70006f-b2f8-4467-ac35-724ce3aa486f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3a70006f-b2f8-4467-ac35-724ce3aa486f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1d7708b-3385-4d25-ac7a-4222ea522c8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}