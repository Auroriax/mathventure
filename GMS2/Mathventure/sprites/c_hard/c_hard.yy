{
    "id": "a7a5c2a2-3da9-41f7-9f8c-139d5dcf4dc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "c_hard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 291,
    "bbox_left": 0,
    "bbox_right": 288,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40ce8c07-e178-40d3-a8c1-781a6363479e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a5c2a2-3da9-41f7-9f8c-139d5dcf4dc8",
            "compositeImage": {
                "id": "8b395c1e-e554-4d62-93ea-a20ecbd37147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ce8c07-e178-40d3-a8c1-781a6363479e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4887ab9e-b245-4653-b5cb-e02a08b62a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ce8c07-e178-40d3-a8c1-781a6363479e",
                    "LayerId": "9432227f-0ad0-436e-a6e8-83ef25a68f01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "9432227f-0ad0-436e-a6e8-83ef25a68f01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7a5c2a2-3da9-41f7-9f8c-139d5dcf4dc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}