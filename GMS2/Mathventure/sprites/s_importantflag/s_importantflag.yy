{
    "id": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_importantflag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebb6db12-7571-4915-a5ce-d57cb3f85124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
            "compositeImage": {
                "id": "1074abc4-86f8-4777-bd24-953ff45a42dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb6db12-7571-4915-a5ce-d57cb3f85124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79237b26-f712-49d7-aa86-7c88d8598542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb6db12-7571-4915-a5ce-d57cb3f85124",
                    "LayerId": "3b0df42f-880a-4126-94ce-b470336bd450"
                }
            ]
        },
        {
            "id": "651869c7-03bc-471d-9980-c2a3c5d974f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
            "compositeImage": {
                "id": "02cb9441-9c8c-47b6-8abb-727f93888c93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "651869c7-03bc-471d-9980-c2a3c5d974f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb729443-8027-4eac-92d2-c3cd1b18161d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "651869c7-03bc-471d-9980-c2a3c5d974f4",
                    "LayerId": "3b0df42f-880a-4126-94ce-b470336bd450"
                }
            ]
        },
        {
            "id": "a516cd8d-53f8-4a27-9ddf-d3c17c1a0fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
            "compositeImage": {
                "id": "466b82fa-1d0b-40ab-9342-085df5a1791d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a516cd8d-53f8-4a27-9ddf-d3c17c1a0fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0170254-6554-4aad-8f9e-810f683d150c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a516cd8d-53f8-4a27-9ddf-d3c17c1a0fde",
                    "LayerId": "3b0df42f-880a-4126-94ce-b470336bd450"
                }
            ]
        },
        {
            "id": "1f808d23-49ab-4841-955a-bd18c4df55f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
            "compositeImage": {
                "id": "300017cf-2a48-4b57-baeb-0fbbe697b1c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f808d23-49ab-4841-955a-bd18c4df55f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e25b0c-40de-4773-9b59-e888df2adcf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f808d23-49ab-4841-955a-bd18c4df55f2",
                    "LayerId": "3b0df42f-880a-4126-94ce-b470336bd450"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "3b0df42f-880a-4126-94ce-b470336bd450",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}