{
    "id": "264f29e5-6d94-46ea-9a98-bc7623025c14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_flagoutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46114612-fa30-4e78-b2af-f6f617118516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "264f29e5-6d94-46ea-9a98-bc7623025c14",
            "compositeImage": {
                "id": "42f6ad31-a826-4e1c-947e-67fa163aee7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46114612-fa30-4e78-b2af-f6f617118516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ecac04-4e49-4d93-8afd-804b61279861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46114612-fa30-4e78-b2af-f6f617118516",
                    "LayerId": "a0411650-28ab-4547-a464-78df08958c35"
                }
            ]
        },
        {
            "id": "91e24a14-ab1a-4571-bec2-3a1abd8daf6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "264f29e5-6d94-46ea-9a98-bc7623025c14",
            "compositeImage": {
                "id": "a75504fa-ac8e-4c35-9fba-f9ec7934ee3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e24a14-ab1a-4571-bec2-3a1abd8daf6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f33b47e-f61c-4674-b197-1bb13a96d8e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e24a14-ab1a-4571-bec2-3a1abd8daf6b",
                    "LayerId": "a0411650-28ab-4547-a464-78df08958c35"
                }
            ]
        },
        {
            "id": "be7febb6-f124-4cf8-b19f-7db7c843ba35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "264f29e5-6d94-46ea-9a98-bc7623025c14",
            "compositeImage": {
                "id": "238ef8e5-d337-4a7d-878c-d7ee586353d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7febb6-f124-4cf8-b19f-7db7c843ba35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11cf015d-7898-4d84-a2ff-8a3a7977c157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7febb6-f124-4cf8-b19f-7db7c843ba35",
                    "LayerId": "a0411650-28ab-4547-a464-78df08958c35"
                }
            ]
        },
        {
            "id": "c9d565c6-28f0-4b91-bd00-b07ec61627ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "264f29e5-6d94-46ea-9a98-bc7623025c14",
            "compositeImage": {
                "id": "2a8ed29c-ee49-4e07-866f-a7f7c56be532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d565c6-28f0-4b91-bd00-b07ec61627ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e29c8d1-5197-4434-ba96-580fe27f9e0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d565c6-28f0-4b91-bd00-b07ec61627ce",
                    "LayerId": "a0411650-28ab-4547-a464-78df08958c35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "a0411650-28ab-4547-a464-78df08958c35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "264f29e5-6d94-46ea-9a98-bc7623025c14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}