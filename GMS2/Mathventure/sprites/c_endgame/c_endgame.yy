{
    "id": "386d1aca-2d57-4768-a5bf-69be6b101b32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "c_endgame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 623,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97a6242f-0357-45d4-8505-fbf16e422c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386d1aca-2d57-4768-a5bf-69be6b101b32",
            "compositeImage": {
                "id": "12100f74-8749-4c20-9368-27592c487080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a6242f-0357-45d4-8505-fbf16e422c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f818fb0-6cc4-4c7c-8a72-e92eb6524af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a6242f-0357-45d4-8505-fbf16e422c37",
                    "LayerId": "fa810687-9c02-4bed-87da-17a1a4769da6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "fa810687-9c02-4bed-87da-17a1a4769da6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "386d1aca-2d57-4768-a5bf-69be6b101b32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 624,
    "xorig": 0,
    "yorig": 0
}