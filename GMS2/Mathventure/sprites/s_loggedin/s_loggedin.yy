{
    "id": "8402686d-0034-4977-9ac8-4568ea262c97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_loggedin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d312b93-4a9f-45e6-81b0-756bec650598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8402686d-0034-4977-9ac8-4568ea262c97",
            "compositeImage": {
                "id": "e8d5cd9b-abe4-44f6-9684-22f4c8bd8165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d312b93-4a9f-45e6-81b0-756bec650598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8652a3e4-e519-4d4d-b0ad-dea033d1e5c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d312b93-4a9f-45e6-81b0-756bec650598",
                    "LayerId": "c430cf5d-d466-4a1a-afbf-bb3b1def3322"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c430cf5d-d466-4a1a-afbf-bb3b1def3322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8402686d-0034-4977-9ac8-4568ea262c97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}