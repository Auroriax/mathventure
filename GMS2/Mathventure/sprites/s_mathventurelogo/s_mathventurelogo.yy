{
    "id": "bada8614-45aa-4966-a96d-330dca4898bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_mathventurelogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 12,
    "bbox_right": 298,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "084f07c2-28a2-46cb-812f-c03e600b30d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bada8614-45aa-4966-a96d-330dca4898bb",
            "compositeImage": {
                "id": "efd182c1-7bf2-4774-b006-35cf97c886b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084f07c2-28a2-46cb-812f-c03e600b30d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c5c657-7172-41f8-be28-35518149acf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084f07c2-28a2-46cb-812f-c03e600b30d7",
                    "LayerId": "f60c99d3-38c5-42a8-bf26-a1cae94bfefc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 81,
    "layers": [
        {
            "id": "f60c99d3-38c5-42a8-bf26-a1cae94bfefc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bada8614-45aa-4966-a96d-330dca4898bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": -1
}