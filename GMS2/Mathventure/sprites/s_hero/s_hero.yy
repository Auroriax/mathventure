{
    "id": "508f8797-c1b0-445e-b714-be7a7bfb5e85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hero",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 7,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "128bdd9f-5929-4f4e-bcbc-93708741fe57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508f8797-c1b0-445e-b714-be7a7bfb5e85",
            "compositeImage": {
                "id": "04c81815-1e01-477f-974a-e70952102ed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "128bdd9f-5929-4f4e-bcbc-93708741fe57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0bbd35-8b77-4337-aa9a-ae19e58034fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128bdd9f-5929-4f4e-bcbc-93708741fe57",
                    "LayerId": "512aba5b-20a1-4146-952f-49e15a7d2edc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "512aba5b-20a1-4146-952f-49e15a7d2edc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "508f8797-c1b0-445e-b714-be7a7bfb5e85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}