{
    "id": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tutflag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ab53e74-d66e-40c3-9664-b004d12f9e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
            "compositeImage": {
                "id": "684ead92-81ce-4a31-836c-58275beb18ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ab53e74-d66e-40c3-9664-b004d12f9e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909b45ef-3a17-4f8e-80cc-34725dc48c8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab53e74-d66e-40c3-9664-b004d12f9e17",
                    "LayerId": "94be2453-4257-475d-b701-faea63344faa"
                }
            ]
        },
        {
            "id": "60b9ee0b-f786-4abc-bf11-f78b442a6317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
            "compositeImage": {
                "id": "f867bf6d-702e-429c-a7b2-709ff55f8671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b9ee0b-f786-4abc-bf11-f78b442a6317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef4e031-4400-466b-a8f3-4f64578221fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b9ee0b-f786-4abc-bf11-f78b442a6317",
                    "LayerId": "94be2453-4257-475d-b701-faea63344faa"
                }
            ]
        },
        {
            "id": "90d93eb1-8a86-4bbf-b6b3-bd2cf2225321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
            "compositeImage": {
                "id": "7a20ec57-a055-4aab-b73b-a335928ce89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d93eb1-8a86-4bbf-b6b3-bd2cf2225321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae84143c-c5ec-48f8-817c-1b21013527a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d93eb1-8a86-4bbf-b6b3-bd2cf2225321",
                    "LayerId": "94be2453-4257-475d-b701-faea63344faa"
                }
            ]
        },
        {
            "id": "8dc67c91-0bdf-4ce4-839a-c787c335a75b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
            "compositeImage": {
                "id": "a4eabcc0-7c6a-4129-a73c-b1362c965a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc67c91-0bdf-4ce4-839a-c787c335a75b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b651643-c21e-4466-8547-fb4413c85927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc67c91-0bdf-4ce4-839a-c787c335a75b",
                    "LayerId": "94be2453-4257-475d-b701-faea63344faa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "94be2453-4257-475d-b701-faea63344faa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}