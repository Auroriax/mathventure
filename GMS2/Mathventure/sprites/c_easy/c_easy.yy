{
    "id": "61532c99-b184-48c5-9a45-95508bf78c40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "c_easy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 478,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c89d30a-0c66-44ea-ab81-c5c096ff3b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61532c99-b184-48c5-9a45-95508bf78c40",
            "compositeImage": {
                "id": "269d46a0-a390-4493-be9b-a963011533f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c89d30a-0c66-44ea-ab81-c5c096ff3b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71646b3f-a9bb-4294-9846-afbc4d8a4c6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c89d30a-0c66-44ea-ab81-c5c096ff3b5c",
                    "LayerId": "fd9c7e8a-99d8-4b45-975e-b222944a7e83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "fd9c7e8a-99d8-4b45-975e-b222944a7e83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61532c99-b184-48c5-9a45-95508bf78c40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 479,
    "xorig": -80,
    "yorig": 0
}