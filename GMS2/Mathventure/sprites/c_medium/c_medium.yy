{
    "id": "d7266bd7-17bc-452d-9bf5-2e671934ed5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "c_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 362,
    "bbox_left": 0,
    "bbox_right": 379,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2cc82d0-7c4a-42ad-8a88-dc38a111dba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7266bd7-17bc-452d-9bf5-2e671934ed5e",
            "compositeImage": {
                "id": "8da417d5-2d94-41f1-85a1-2362e3d32d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2cc82d0-7c4a-42ad-8a88-dc38a111dba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73d78fe9-dc9d-40df-909d-003ab8586540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2cc82d0-7c4a-42ad-8a88-dc38a111dba7",
                    "LayerId": "a27c67dc-8f55-4d0a-9ea7-7ce7350d3c15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 363,
    "layers": [
        {
            "id": "a27c67dc-8f55-4d0a-9ea7-7ce7350d3c15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7266bd7-17bc-452d-9bf5-2e671934ed5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 380,
    "xorig": 0,
    "yorig": -37
}