{
    "id": "b06f6a47-68e1-4ed0-9b47-632981ecc633",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_subtractsqempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76fbd7c8-b7bd-4656-8932-fd8be928a94c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b06f6a47-68e1-4ed0-9b47-632981ecc633",
            "compositeImage": {
                "id": "8b90b1a0-980d-4781-9b98-33b549c16a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fbd7c8-b7bd-4656-8932-fd8be928a94c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64daaba7-273b-421d-b06a-4270c2b898c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fbd7c8-b7bd-4656-8932-fd8be928a94c",
                    "LayerId": "ae27f619-d637-4c7d-84eb-467656cda512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ae27f619-d637-4c7d-84eb-467656cda512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b06f6a47-68e1-4ed0-9b47-632981ecc633",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}