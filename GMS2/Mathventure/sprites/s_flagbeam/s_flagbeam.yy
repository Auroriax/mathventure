{
    "id": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_flagbeam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 12,
    "bbox_right": 80,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89e5fa40-a97a-4676-b452-7d0318520ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "1f2a6112-398f-432f-9f0c-56834c84c692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e5fa40-a97a-4676-b452-7d0318520ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c2abdf-7449-4bdc-9932-99cbac72292f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e5fa40-a97a-4676-b452-7d0318520ad3",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "3b1d325e-453e-470f-8452-c15243b14f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "35bb1d72-13e1-40d0-851f-25592aab0d37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b1d325e-453e-470f-8452-c15243b14f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9bdf95-0b43-4977-bbf1-8d95bf24137f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b1d325e-453e-470f-8452-c15243b14f8f",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "5b07cee9-b26b-4f9c-8e9c-4d235d93e343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "ad8caea1-0636-44aa-be88-d9d291f66d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b07cee9-b26b-4f9c-8e9c-4d235d93e343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc698bd-d63f-453b-a310-6f322a75b512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b07cee9-b26b-4f9c-8e9c-4d235d93e343",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "1764e730-0af4-4863-b1bf-acef712b6bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "a0a01269-7d41-4fda-801a-04f787d29bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1764e730-0af4-4863-b1bf-acef712b6bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc040c2-8d91-4d17-97b1-079077ff53f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1764e730-0af4-4863-b1bf-acef712b6bab",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "9c7d6b52-6e01-4b07-b01d-604e4739fd26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "add058b9-e6fd-4705-bd86-d99f74bc2df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c7d6b52-6e01-4b07-b01d-604e4739fd26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28fdd8db-47c0-4c59-81de-2b925e425f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c7d6b52-6e01-4b07-b01d-604e4739fd26",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "2179e7ac-579c-47ba-9552-54f760474100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "bd07fea4-f676-41d6-a63c-238797120d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2179e7ac-579c-47ba-9552-54f760474100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb1148e-c100-4d21-a2bc-9e4c938287be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2179e7ac-579c-47ba-9552-54f760474100",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "4913bce5-861f-4650-8b2a-cc9d363e91f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "9bf92489-1689-43f9-81a3-47115367a76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4913bce5-861f-4650-8b2a-cc9d363e91f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed333305-11d7-4600-b323-77d8bf064aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4913bce5-861f-4650-8b2a-cc9d363e91f5",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "644873b1-4d93-4c59-90e4-2c0fa39238ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "c2293606-d02c-4194-9621-3c9a76f7e88c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "644873b1-4d93-4c59-90e4-2c0fa39238ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bde9f3-b5a0-47c6-afd2-0c5425ab4e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "644873b1-4d93-4c59-90e4-2c0fa39238ff",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "fd6ec523-4f9c-47d7-a98f-c6c2b1b32d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "391ca80a-842f-444d-893b-8fb727ddd490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6ec523-4f9c-47d7-a98f-c6c2b1b32d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ac5e8d-c80d-4ac3-b407-afa0a2684fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6ec523-4f9c-47d7-a98f-c6c2b1b32d8d",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "d3346843-36b9-48c0-a63d-28d9ddfb9f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "44513b2e-35df-4dad-aab7-bf82278410e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3346843-36b9-48c0-a63d-28d9ddfb9f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a3a28f-bc32-46c3-83a9-2fb24cfeb236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3346843-36b9-48c0-a63d-28d9ddfb9f5b",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        },
        {
            "id": "6309eb6a-feea-424d-83a5-7369b19270aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "compositeImage": {
                "id": "02190fe4-1c88-4b2d-8df3-16cecb06b895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6309eb6a-feea-424d-83a5-7369b19270aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5552a94-5be6-4a0f-981c-ebc5a6e958e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6309eb6a-feea-424d-83a5-7369b19270aa",
                    "LayerId": "b7d59322-9c52-4448-a31b-b0ded4de01b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "b7d59322-9c52-4448-a31b-b0ded4de01b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}