{
    "id": "5fcad098-27e5-4f9c-97ff-b7c19e7c18d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_goal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f351ff82-2a8e-4a1a-bf94-4ef2c00a664c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fcad098-27e5-4f9c-97ff-b7c19e7c18d6",
            "compositeImage": {
                "id": "b90d70bc-8eb3-4554-b5ae-87a509716dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f351ff82-2a8e-4a1a-bf94-4ef2c00a664c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82a98a8b-2f93-4cc2-90d1-ddcb9367357f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f351ff82-2a8e-4a1a-bf94-4ef2c00a664c",
                    "LayerId": "9d8553f5-ace0-46ae-b8db-c97baee02ead"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "9d8553f5-ace0-46ae-b8db-c97baee02ead",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fcad098-27e5-4f9c-97ff-b7c19e7c18d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}