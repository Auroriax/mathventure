{
    "id": "2e72bf21-3a15-4e28-a57b-0e72f1b9a309",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_numberadd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82a2c173-f1c6-488c-ac5f-6e93af6e3476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e72bf21-3a15-4e28-a57b-0e72f1b9a309",
            "compositeImage": {
                "id": "4c5ecc28-1d61-4b35-95d3-2724724e6b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a2c173-f1c6-488c-ac5f-6e93af6e3476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c821ad-587d-417f-b06c-041bfeedd4c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a2c173-f1c6-488c-ac5f-6e93af6e3476",
                    "LayerId": "88dff08b-8cd9-4b59-bb17-ab69e9801e8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "88dff08b-8cd9-4b59-bb17-ab69e9801e8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e72bf21-3a15-4e28-a57b-0e72f1b9a309",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}