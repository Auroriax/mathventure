{
    "id": "b6668b84-cd2e-4f17-b5ca-366472c5108e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_minusyeah",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad7d93cf-1719-4310-a16e-b102331713c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6668b84-cd2e-4f17-b5ca-366472c5108e",
            "compositeImage": {
                "id": "e6853531-a0aa-4f0f-8133-87a5b34ba073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad7d93cf-1719-4310-a16e-b102331713c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "614d4699-54a9-4a53-9dc4-1cdcc9054991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad7d93cf-1719-4310-a16e-b102331713c3",
                    "LayerId": "8e35cc7d-96e7-4321-9e6c-139d213960bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "8e35cc7d-96e7-4321-9e6c-139d213960bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6668b84-cd2e-4f17-b5ca-366472c5108e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}