{
    "id": "1db5e248-e8cd-4dcc-97db-35a869a8cbb3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_trapunactive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 4,
    "bbox_right": 18,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a20a0766-35d5-484f-aeb9-19fa23c1a206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1db5e248-e8cd-4dcc-97db-35a869a8cbb3",
            "compositeImage": {
                "id": "52a2d713-aad3-4293-8d64-29e86fd613de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a20a0766-35d5-484f-aeb9-19fa23c1a206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a0bb943-1b39-4865-9fec-c5e2522acab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a20a0766-35d5-484f-aeb9-19fa23c1a206",
                    "LayerId": "496ccb0b-98c9-47dc-b599-1f998e035efe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "496ccb0b-98c9-47dc-b599-1f998e035efe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1db5e248-e8cd-4dcc-97db-35a869a8cbb3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}