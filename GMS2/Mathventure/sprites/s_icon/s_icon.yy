{
    "id": "6bfb7711-c195-4da0-b2d2-7adf0100a193",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 22,
    "bbox_right": 43,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c5e0386-4b89-4a5f-9dd0-838f67035192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfb7711-c195-4da0-b2d2-7adf0100a193",
            "compositeImage": {
                "id": "aa695941-320c-4e0d-9f23-a0d9fd4beb2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5e0386-4b89-4a5f-9dd0-838f67035192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0abf38a3-e0bc-4b58-b65d-8261f79122eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5e0386-4b89-4a5f-9dd0-838f67035192",
                    "LayerId": "7f6d15bc-f566-420e-9fc1-5bd84588a097"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f6d15bc-f566-420e-9fc1-5bd84588a097",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bfb7711-c195-4da0-b2d2-7adf0100a193",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}