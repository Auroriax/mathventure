{
    "id": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_greenflag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f69fbdd0-2045-4c07-a19f-8813f2ec1b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
            "compositeImage": {
                "id": "3b66eb0d-96cd-4788-bbe9-471afbe30d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f69fbdd0-2045-4c07-a19f-8813f2ec1b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fa6f97-36b0-4d4c-8ab5-3d2a260035c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69fbdd0-2045-4c07-a19f-8813f2ec1b2d",
                    "LayerId": "6b49878e-17fd-4d48-b1b3-c68e87e5c31a"
                }
            ]
        },
        {
            "id": "471eb17e-8263-4664-87eb-6f2e79a5b031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
            "compositeImage": {
                "id": "25fceffa-61d8-4467-a076-d6a381f79130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471eb17e-8263-4664-87eb-6f2e79a5b031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3619bf5-d1a0-451d-8e95-1fa3aca418de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471eb17e-8263-4664-87eb-6f2e79a5b031",
                    "LayerId": "6b49878e-17fd-4d48-b1b3-c68e87e5c31a"
                }
            ]
        },
        {
            "id": "eb4211a7-4d22-432a-a358-c0ce84bf551f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
            "compositeImage": {
                "id": "79a61866-ee08-4289-bbf2-a82c2d9acc9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb4211a7-4d22-432a-a358-c0ce84bf551f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49055203-ced1-46fc-9da5-11153728d851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb4211a7-4d22-432a-a358-c0ce84bf551f",
                    "LayerId": "6b49878e-17fd-4d48-b1b3-c68e87e5c31a"
                }
            ]
        },
        {
            "id": "1a11dd95-b20d-4872-9d9c-47f423321195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
            "compositeImage": {
                "id": "f54ebe12-759f-4ff6-aa44-f3344d7c9438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a11dd95-b20d-4872-9d9c-47f423321195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408c6702-9f98-4428-b689-1b045de1128e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a11dd95-b20d-4872-9d9c-47f423321195",
                    "LayerId": "6b49878e-17fd-4d48-b1b3-c68e87e5c31a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "6b49878e-17fd-4d48-b1b3-c68e87e5c31a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}