{
    "id": "68ae4f7c-976c-47a2-b392-3fe714867d04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_trapactive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8389f6b9-cbf6-4d04-96dc-e19c8758399e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ae4f7c-976c-47a2-b392-3fe714867d04",
            "compositeImage": {
                "id": "4b5ee02a-c595-457c-bcf4-79a8a9eeb456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8389f6b9-cbf6-4d04-96dc-e19c8758399e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ce9256-49e9-4c4c-9a83-2a9c23add570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8389f6b9-cbf6-4d04-96dc-e19c8758399e",
                    "LayerId": "30216014-70c2-42c1-8e0d-e28aa7618d0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "30216014-70c2-42c1-8e0d-e28aa7618d0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ae4f7c-976c-47a2-b392-3fe714867d04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}