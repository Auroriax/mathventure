{
    "id": "c4c3531b-117f-48d4-b8e6-9855068f0221",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_whiteflag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50635dd8-96f0-4d59-b36c-cce37a9b0855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c3531b-117f-48d4-b8e6-9855068f0221",
            "compositeImage": {
                "id": "577c03ee-1278-4d4f-bbb0-f57ab290fd09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50635dd8-96f0-4d59-b36c-cce37a9b0855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2e50d4-f1b7-4a35-8124-72eb37b3ef47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50635dd8-96f0-4d59-b36c-cce37a9b0855",
                    "LayerId": "a314c8f3-6d40-431f-a925-8da25eff76c8"
                }
            ]
        },
        {
            "id": "fe45c9e9-35f5-49e5-8b87-0f47a8ece074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c3531b-117f-48d4-b8e6-9855068f0221",
            "compositeImage": {
                "id": "8bf05bf9-4b3b-477e-a4a7-bf6c92cd8db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe45c9e9-35f5-49e5-8b87-0f47a8ece074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "986475af-2c17-4f3c-8045-be03cc1085db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe45c9e9-35f5-49e5-8b87-0f47a8ece074",
                    "LayerId": "a314c8f3-6d40-431f-a925-8da25eff76c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "a314c8f3-6d40-431f-a925-8da25eff76c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4c3531b-117f-48d4-b8e6-9855068f0221",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}