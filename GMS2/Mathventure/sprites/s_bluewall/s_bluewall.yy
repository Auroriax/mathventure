{
    "id": "488e8ed4-087b-474f-b5f8-2a36b0cc2c0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bluewall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "508dd9af-c474-4ba0-953c-1f72e31a9b66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "488e8ed4-087b-474f-b5f8-2a36b0cc2c0f",
            "compositeImage": {
                "id": "c9205c0f-2fab-433f-90b8-6649dfa2d5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "508dd9af-c474-4ba0-953c-1f72e31a9b66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e754cb27-0207-4f6d-9bf0-653ee62ade2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "508dd9af-c474-4ba0-953c-1f72e31a9b66",
                    "LayerId": "92b6e353-1d1c-48cd-a79d-ddd9ca9f1c16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "92b6e353-1d1c-48cd-a79d-ddd9ca9f1c16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "488e8ed4-087b-474f-b5f8-2a36b0cc2c0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}