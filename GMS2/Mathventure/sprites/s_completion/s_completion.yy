{
    "id": "852aff7c-dfaf-455f-8362-35ce04120660",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_completion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 174,
    "bbox_left": 3,
    "bbox_right": 197,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33f7aae7-78e7-48e5-83d9-4d92627446f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852aff7c-dfaf-455f-8362-35ce04120660",
            "compositeImage": {
                "id": "d43f230f-a400-451c-b005-7cef1410e8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f7aae7-78e7-48e5-83d9-4d92627446f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e5f332-a078-462c-bf0f-18ce5d1fadba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f7aae7-78e7-48e5-83d9-4d92627446f2",
                    "LayerId": "c152316e-74a7-47df-97d0-286ea013b2b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "c152316e-74a7-47df-97d0-286ea013b2b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "852aff7c-dfaf-455f-8362-35ce04120660",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}