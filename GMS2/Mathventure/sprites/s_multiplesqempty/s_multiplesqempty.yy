{
    "id": "e424a7ed-c561-4859-be94-575c7a23ec2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_multiplesqempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9379f595-d4d2-4a0c-80e3-5db75b86dff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e424a7ed-c561-4859-be94-575c7a23ec2b",
            "compositeImage": {
                "id": "5c1aaa60-7141-4cbb-b518-5d7ca7063ac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9379f595-d4d2-4a0c-80e3-5db75b86dff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7bf337-edf4-43a6-b824-d68fa57deccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9379f595-d4d2-4a0c-80e3-5db75b86dff4",
                    "LayerId": "5ff330a5-351b-48fb-946d-44640f8a0bdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "5ff330a5-351b-48fb-946d-44640f8a0bdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e424a7ed-c561-4859-be94-575c7a23ec2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}