{
    "id": "993b0c0b-68d4-4ff4-8c19-158616c2f6fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_coolMathSplash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c390444-93bb-451c-a7f4-8cbbb95160e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "993b0c0b-68d4-4ff4-8c19-158616c2f6fe",
            "compositeImage": {
                "id": "d49752d3-a871-471f-825e-ba950d02b5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c390444-93bb-451c-a7f4-8cbbb95160e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6334946-61ae-4053-a6c7-c2dfe115afbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c390444-93bb-451c-a7f4-8cbbb95160e9",
                    "LayerId": "97131c05-f15d-480a-b45e-f987d5db0855"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "97131c05-f15d-480a-b45e-f987d5db0855",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "993b0c0b-68d4-4ff4-8c19-158616c2f6fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}