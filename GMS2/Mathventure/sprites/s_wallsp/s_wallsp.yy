{
    "id": "acb19995-868f-4150-954e-efee2418297a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wallsp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13be86f5-22b5-4cd4-9149-f2c40e79d5cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acb19995-868f-4150-954e-efee2418297a",
            "compositeImage": {
                "id": "76c4197b-207d-4846-97c8-b42a29f9c7a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13be86f5-22b5-4cd4-9149-f2c40e79d5cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f5cc714-09e9-4740-a065-9dccf3a7c92b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13be86f5-22b5-4cd4-9149-f2c40e79d5cf",
                    "LayerId": "24d558fd-eef3-465f-9b13-4b6a1d449e83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "24d558fd-eef3-465f-9b13-4b6a1d449e83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acb19995-868f-4150-954e-efee2418297a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}