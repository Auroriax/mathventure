{
    "id": "0eae26cf-22fc-4002-b757-22816261a6bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37485223-6351-4016-9c7e-7d44bcbcba96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eae26cf-22fc-4002-b757-22816261a6bd",
            "compositeImage": {
                "id": "14f1625c-ca3d-4dfd-bfb4-8e4e8546ee06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37485223-6351-4016-9c7e-7d44bcbcba96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f632459-8bba-4277-982b-41dbe3a37a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37485223-6351-4016-9c7e-7d44bcbcba96",
                    "LayerId": "0226d69a-dcf8-4246-9b37-be5fc118cf72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0226d69a-dcf8-4246-9b37-be5fc118cf72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0eae26cf-22fc-4002-b757-22816261a6bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}