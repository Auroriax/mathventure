{
    "id": "3bbe79cf-66e5-4c7c-aec1-0b8c9569b763",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_minusrdempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "766a72a3-b71a-46e4-9da0-2da819b97cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bbe79cf-66e5-4c7c-aec1-0b8c9569b763",
            "compositeImage": {
                "id": "0820ed71-6944-4641-bc75-62ae6ed7d7e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766a72a3-b71a-46e4-9da0-2da819b97cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c48a42-f70e-4eab-9454-3ce776782d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766a72a3-b71a-46e4-9da0-2da819b97cb8",
                    "LayerId": "6b942c16-a49e-40e6-a86b-72a8106c279c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6b942c16-a49e-40e6-a86b-72a8106c279c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bbe79cf-66e5-4c7c-aec1-0b8c9569b763",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}