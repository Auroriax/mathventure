{
    "id": "e24d7b18-7698-4938-9d28-c637ed1b2682",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_addsqempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c41c13a-4fc7-4e27-b37f-16139183dc94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e24d7b18-7698-4938-9d28-c637ed1b2682",
            "compositeImage": {
                "id": "4d46d45d-a5d7-43c0-97d4-623e4bc67f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c41c13a-4fc7-4e27-b37f-16139183dc94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edbe2468-db00-4663-bc12-1551b2282445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c41c13a-4fc7-4e27-b37f-16139183dc94",
                    "LayerId": "a1c0274c-466f-46b2-8898-261c94d2f4a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a1c0274c-466f-46b2-8898-261c94d2f4a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e24d7b18-7698-4938-9d28-c637ed1b2682",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}