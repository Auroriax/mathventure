{
    "id": "0d9154a4-998f-4a90-bcb2-18cb6722ef3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d319e58a-f964-4c0d-8f85-a99cae45e2e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d9154a4-998f-4a90-bcb2-18cb6722ef3f",
            "compositeImage": {
                "id": "a185e307-1fd4-4d9e-87a3-6845fbf8aa97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d319e58a-f964-4c0d-8f85-a99cae45e2e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea7f033-5b74-4fbf-b262-5e656ca5cddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d319e58a-f964-4c0d-8f85-a99cae45e2e1",
                    "LayerId": "46725b94-c19e-4034-a587-a2e12b1dc50a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "46725b94-c19e-4034-a587-a2e12b1dc50a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d9154a4-998f-4a90-bcb2-18cb6722ef3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}