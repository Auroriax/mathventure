{
    "id": "6d6a138a-6b88-43dc-84a4-cc345dfb22d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 623,
    "bbox_left": 0,
    "bbox_right": 623,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6acdbbd-d34b-49bf-b5c7-3c269c5bcbe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6a138a-6b88-43dc-84a4-cc345dfb22d8",
            "compositeImage": {
                "id": "f5314097-169d-4404-ab14-932c40e337f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6acdbbd-d34b-49bf-b5c7-3c269c5bcbe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c0c187-18f5-41d6-a974-5e00f149391e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6acdbbd-d34b-49bf-b5c7-3c269c5bcbe5",
                    "LayerId": "e1ef7075-9082-4572-a8d6-728d3be1044a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 624,
    "layers": [
        {
            "id": "e1ef7075-9082-4572-a8d6-728d3be1044a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d6a138a-6b88-43dc-84a4-cc345dfb22d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 624,
    "xorig": 0,
    "yorig": 0
}