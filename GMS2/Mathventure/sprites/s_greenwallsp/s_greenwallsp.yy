{
    "id": "2a34092d-bf55-4e83-bbb9-e8f0f9511937",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_greenwallsp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8465afd6-cfac-4edc-b27f-fb7bde0dc774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a34092d-bf55-4e83-bbb9-e8f0f9511937",
            "compositeImage": {
                "id": "ea967a08-9039-45cf-bcca-9f0b7471b547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8465afd6-cfac-4edc-b27f-fb7bde0dc774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49e808a-890d-448f-84e5-86158f1121f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8465afd6-cfac-4edc-b27f-fb7bde0dc774",
                    "LayerId": "3f7c3247-9e41-4a85-9178-ac10dd3592a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3f7c3247-9e41-4a85-9178-ac10dd3592a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a34092d-bf55-4e83-bbb9-e8f0f9511937",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}