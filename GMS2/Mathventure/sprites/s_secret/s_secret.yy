{
    "id": "6d654916-7019-4251-909d-7cec0026ac83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_secret",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b42dd5a-cd33-47d7-8880-e0d71d83fe7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d654916-7019-4251-909d-7cec0026ac83",
            "compositeImage": {
                "id": "b79cbc04-0079-4dde-86c1-f0c9c8636662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b42dd5a-cd33-47d7-8880-e0d71d83fe7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d7bee02-397a-482d-90a3-10f155eb8670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b42dd5a-cd33-47d7-8880-e0d71d83fe7a",
                    "LayerId": "ecede376-4e17-4c0b-af62-49e12aef5bbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ecede376-4e17-4c0b-af62-49e12aef5bbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d654916-7019-4251-909d-7cec0026ac83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 0,
    "yorig": 0
}