{
    "id": "d19532bc-9726-411e-b9f8-49d983fbd115",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_notloggedin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e994f1db-ac6c-4976-ae33-f9e6a79c66d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d19532bc-9726-411e-b9f8-49d983fbd115",
            "compositeImage": {
                "id": "cfcedc59-8135-4ac0-a490-dddeb42e4777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e994f1db-ac6c-4976-ae33-f9e6a79c66d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3a3ec3-66b0-4d13-a06f-09c458ad13a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e994f1db-ac6c-4976-ae33-f9e6a79c66d6",
                    "LayerId": "5dec814c-c41b-40b3-8048-40e4b9d6d478"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "5dec814c-c41b-40b3-8048-40e4b9d6d478",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d19532bc-9726-411e-b9f8-49d983fbd115",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}