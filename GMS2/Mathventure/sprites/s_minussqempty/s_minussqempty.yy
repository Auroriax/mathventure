{
    "id": "457f74c4-9f9f-4d52-897e-ab3a457011fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_minussqempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ae4a1ac-eec5-439f-9d8c-f210aa934cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "457f74c4-9f9f-4d52-897e-ab3a457011fe",
            "compositeImage": {
                "id": "e9785305-9b70-4838-8c86-e84b47aee721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae4a1ac-eec5-439f-9d8c-f210aa934cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb3f8295-959d-4356-91c0-75d40d2b6080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae4a1ac-eec5-439f-9d8c-f210aa934cae",
                    "LayerId": "088a1686-4171-4149-98d8-0c0a39b1119f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "088a1686-4171-4149-98d8-0c0a39b1119f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "457f74c4-9f9f-4d52-897e-ab3a457011fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}