{
    "id": "b85ced4d-612d-4067-8e51-56a1d55b9856",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "terminal12",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fdee3ea3-20f6-4c84-95e1-c1f7ce4c90cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 27,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 201,
                "y": 86
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8147df7b-6e27-4461-a0dc-576284965913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 168,
                "y": 115
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1518874a-d5fe-4ed7-acd4-503275adc2ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 136,
                "y": 115
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5845f974-c4f1-441c-a7b8-dd34a27dca71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 30
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f5eba1ee-b47c-4ccb-b329-3ef0bace3c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 59,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "be5c9bf5-5f6f-4111-8e36-2e602f8b65ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3139928b-1934-418a-b7a0-8f1d71cdd79a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 36,
                "y": 30
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "91ae0065-8651-420a-93e1-b50da45adead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 186,
                "y": 115
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "72d47dbf-3424-4a00-98a8-45bcd5763afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 115
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4a742c26-d974-4c2c-a143-11607fa351c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 115
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "02548915-8b22-4f62-a72e-f059fd74d23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 115
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8e0aa45f-f226-4d35-95b6-c6b35a1b760f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d8195020-56af-48b6-b368-ccc4331d4618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 152,
                "y": 115
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "77b86a6f-b11f-45bd-b81b-c832a526b52a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 105,
                "y": 115
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2b08aff3-f434-4bd8-a242-f6743fcaa20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 176,
                "y": 115
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6ec84cb0-7937-4b0d-bb69-42cb717841c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 34,
                "y": 115
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a48ccb89-b6c4-49de-986c-c3d338d9a6c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 151,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "54e07dbb-aa20-4b7a-afbe-60297d626328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 89,
                "y": 115
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4d893388-b7be-42ba-8252-3b9ca189fe31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 112,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "571d082e-7097-4bbb-818a-bfcee0ec620d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a7110b88-d6ed-40bf-b075-51a606a41123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 229,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9665ba21-d793-4f38-823a-7b8282fc39cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 86
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e4db188a-c9a6-4d34-992f-ee4303044f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 216,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d3a46172-6112-450a-a367-eaf8a4122703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 190,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f6ef5fb0-ccbe-4026-b9f5-262b6c8631b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 99,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1d81606d-c4fd-4410-abf7-189bbb2592af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 164,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "06f89926-ce0a-4b9b-bca3-0c9d451dfbd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 172,
                "y": 115
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0caf6e4c-0b92-407a-be44-cd16c0a5570f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 148,
                "y": 115
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3951c803-8a55-4f64-a909-621e2ca516b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 177,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "36d24375-698d-4b2c-943a-f58d2189b4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 238,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ebc09a77-414e-4169-8cc7-a127e4a4c2d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 164,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "522c120c-0209-49d1-8aa2-48340906ee28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 242,
                "y": 58
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "70cd9358-379d-44fe-8afb-8c5c54c40884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b5b15fb4-4187-4664-af69-6417058933f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "baadc727-8b11-4f8b-8194-f25820de9838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 217,
                "y": 30
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2ec5a57c-df37-44b2-b846-effe68253bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3c39ce82-82ac-4de2-a832-fe2ca890947b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 68,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5f8a3808-4911-4dc1-95ac-63374a047769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 32,
                "y": 58
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cf8dbde0-49dc-4285-8a99-3fcc1875b5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 72,
                "y": 58
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f2cb5769-40f7-43ef-abb5-df721aaf1969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a5b73a05-30df-4815-aad3-25a581e9f207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 17,
                "y": 58
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0f408e42-fc2c-4e99-9562-ae8e7a803130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 164,
                "y": 115
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "08a26f44-21d8-4d2e-a0e1-76b705dbca96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 190,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "991a2cb3-aa7e-4e1e-8b1a-468e18ee7ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 30
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d9d9d7df-45d4-42ba-b8c1-f903f2b535c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 140,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5a372a5b-0be2-4b15-b240-e7841cb667f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 30
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "aebdc038-6bdd-471c-87b9-e4387c15df6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "57a79408-165a-49fa-b78d-36ca23b84191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "58ccbb93-ba6c-435c-b934-f84f47cd997c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 202,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "44f1f1bb-d0ea-471e-8df8-a386ab2af043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8a094698-964d-454d-8993-0bd8cb96b41c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 132,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f9fab6eb-8d06-4fb4-94b4-01222d42ad0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9d4b99ba-e098-47d6-891e-526f6a90efa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 116,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "90baf676-781a-45fd-9ffb-45eb5f53c748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 187,
                "y": 30
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ef75ded7-da46-43b3-b224-e7b583f23119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f130e548-289f-48c3-886a-7a9014409190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "18c99fdb-49ca-4eac-82e3-84a82ea29f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8bbfab99-fee3-44cb-8600-9ac6b974b7d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3767a23d-b8a4-4f7c-b42a-53bda8b8f827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 84,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9d288679-3911-465a-b5e9-7ecb1dae0a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 120,
                "y": 115
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4eeffe5a-2319-45d5-84f0-7e2e6cf34bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 43,
                "y": 115
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1ecb48ac-5a10-4b9f-816c-7ee2b02321e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 114,
                "y": 115
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "64932f13-47b1-40b5-bc6c-03752f5b3291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 70,
                "y": 115
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "db2e0853-e52b-4240-84a1-33bef3c63901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0ea9f064-a8a9-4408-a59d-c449638f8fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 180,
                "y": 115
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fe895fed-4dd7-4176-8248-d279df484de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 86
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d87c74d1-50c4-4529-b79c-c97cb96177e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4be4a961-0c8f-449e-9985-869fd51acb56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 152,
                "y": 86
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "644df4fb-b23a-4f97-b176-1e0e883d7f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 203,
                "y": 58
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "67d35d23-30c5-4856-a3af-05091d7f5743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 138,
                "y": 58
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f886ac04-d517-4cc6-a30f-8aa788830b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 115
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fc590608-2e8c-4e0e-9dc1-d57e032dde10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 148,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "eaca4fa6-f4df-45f4-a260-e1ea907626c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 116,
                "y": 86
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4eff5cd3-d25f-40ab-9d90-8da05a5c1704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 160,
                "y": 115
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cf200d0e-7d6e-4ba7-a1a0-d6d35422034b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -1,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 115
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f1a66432-86fd-4f0d-a340-37d4ef8b09da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 128,
                "y": 86
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e7520196-a646-4aa1-8287-11877ee318af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 156,
                "y": 115
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "45f42f93-f0cf-43eb-a7fd-59d57bcb5f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f57b6a37-8fa2-47d6-9a6c-dd0d2dc2cc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 104,
                "y": 86
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c3394392-0575-4d44-9d78-ab1e78a02cd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 125,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "687c648c-c29e-4672-bb48-27e14dbea0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 161,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c3a7dbf6-46f8-4b32-84e6-85ab76790b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 174,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "72d7884d-b815-4696-ae55-456be0869ee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 115
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1147cb0e-6d67-4f4d-80e7-5332f78191c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 80,
                "y": 86
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "47eb174e-620a-435d-8b0f-92311a87bb1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 52,
                "y": 115
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c139649a-f674-42cd-a3f7-ccc8e476978a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7136ab72-c026-450c-9371-8996c00c1207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 58
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2099d2ef-e6ef-41b9-8e8a-0b5acac3f8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1a2609fb-7918-46e6-8aba-c854ce1c471f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 86,
                "y": 58
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "43c0a89a-a916-4d7f-af9e-9c78bce53a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 47,
                "y": 58
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7159d8f7-d00b-4073-a350-01f23e8c66a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 92,
                "y": 86
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ab1a8d37-57fc-498b-a234-8e97f75cde74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 115
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "32b2f06e-9070-4c8c-a678-98774837e957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 27,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 144,
                "y": 115
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0d29b724-aedf-4800-8c19-470ce3750a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 115
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9b4e8bce-a2a1-4439-ab50-3705e328d93b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 224,
                "y": 86
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "4014bcfa-47f4-4393-b174-ff0288437552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 15,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 210,
                "y": 86
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}