{
    "id": "b87637cb-b3d9-473c-9a2a-6f2deac99df6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "dj48",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Taurus Mono Outline",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f52c6e79-f659-451a-bc97-a5ad6bafb7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 64,
                "offset": 0,
                "shift": 53,
                "w": 53,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8cecf5ae-733d-4eb5-9051-e690d63f115e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 65,
                "offset": 21,
                "shift": 54,
                "w": 10,
                "x": 570,
                "y": 203
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0cb03f7b-7fe3-4f4a-9dbe-33ce147cae9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 16,
                "shift": 54,
                "w": 23,
                "x": 519,
                "y": 203
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cfbd0735-deab-41b2-95ae-b2a136cf7e10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 65,
                "offset": 0,
                "shift": 54,
                "w": 52,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d089d306-057d-4a20-983f-66cf467c64e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 39,
                "x": 253,
                "y": 136
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e49e29b5-36fa-4192-8458-a32211ee28cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 65,
                "offset": 2,
                "shift": 54,
                "w": 49,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2cc08447-a5f1-4f2f-9a10-6b0c7a563b30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 65,
                "offset": 8,
                "shift": 54,
                "w": 41,
                "x": 706,
                "y": 69
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9e681df4-ed8c-4375-8711-a4489f840af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 21,
                "shift": 54,
                "w": 9,
                "x": 605,
                "y": 203
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ff4d4611-b2c6-4252-8190-5623ce120256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 65,
                "offset": 16,
                "shift": 54,
                "w": 19,
                "x": 432,
                "y": 203
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f53d07cb-24e6-4a67-9c11-ad677656b5e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 65,
                "offset": 16,
                "shift": 54,
                "w": 19,
                "x": 453,
                "y": 203
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f940f5d1-a6a2-4473-b65c-c1278fdcc9ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 7,
                "shift": 54,
                "w": 36,
                "x": 348,
                "y": 203
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4a2e04c7-f297-4f43-ab2d-4639b25c6bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 60,
                "offset": 9,
                "shift": 54,
                "w": 34,
                "x": 37,
                "y": 203
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d83dd072-1106-4013-acdc-7ed17567c5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 65,
                "offset": 18,
                "shift": 54,
                "w": 15,
                "x": 474,
                "y": 203
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5b9859b7-2749-4e78-9c8f-00c97633beed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 8,
                "shift": 54,
                "w": 34,
                "x": 312,
                "y": 203
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "23506ad9-5a58-43ea-b070-c5a67c3e6f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 65,
                "offset": 21,
                "shift": 54,
                "w": 10,
                "x": 558,
                "y": 203
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f0b51c52-6ace-494a-b4c2-7c6e27b257ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 42,
                "x": 443,
                "y": 69
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d4a2fba1-b2f8-4ddf-9ef6-a0e91761477c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 42,
                "x": 179,
                "y": 69
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "50e91dc0-191e-4fd4-9ab1-91e7570a2f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 65,
                "offset": 11,
                "shift": 54,
                "w": 33,
                "x": 963,
                "y": 136
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f95be209-6c22-454b-86aa-02fb31f03c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 65,
                "offset": 8,
                "shift": 54,
                "w": 37,
                "x": 773,
                "y": 136
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e30801fc-956b-4548-ac08-6d52d47d2ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 65,
                "offset": 8,
                "shift": 54,
                "w": 39,
                "x": 212,
                "y": 136
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d5377009-c86e-4b50-8590-265a172a0762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 40,
                "x": 170,
                "y": 136
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dca428ad-fcef-4b2e-b928-3549420348ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 65,
                "offset": 8,
                "shift": 54,
                "w": 36,
                "x": 812,
                "y": 136
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "93bbfd12-ef5e-448b-b1b2-f07614b7dea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 38,
                "x": 376,
                "y": 136
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "37c9cb36-df2c-477e-b61b-0c3156a1b122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 65,
                "offset": 11,
                "shift": 54,
                "w": 37,
                "x": 656,
                "y": 136
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "33c936aa-a624-4d0b-8a92-b32908240ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 267,
                "y": 69
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8d138cf4-e756-4ffd-bad5-edcd631cdbce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 38,
                "x": 496,
                "y": 136
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "974abca2-61c8-4030-9123-579245bb0b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 59,
                "offset": 21,
                "shift": 54,
                "w": 10,
                "x": 582,
                "y": 203
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "002bfe54-100f-431d-b819-adc951f05d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 64,
                "offset": 18,
                "shift": 54,
                "w": 12,
                "x": 544,
                "y": 203
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "eddaa851-e394-49d4-8330-76c6a75bb3c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 64,
                "offset": 12,
                "shift": 54,
                "w": 29,
                "x": 173,
                "y": 203
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1c87b7f5-990a-4cf7-a653-279932aab77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 57,
                "offset": 8,
                "shift": 54,
                "w": 34,
                "x": 106,
                "y": 203
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b53a88c1-39ee-414d-8eff-7e97532b25dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 64,
                "offset": 12,
                "shift": 54,
                "w": 29,
                "x": 142,
                "y": 203
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b5d88b08-1011-4f16-b84a-58e8cd4606d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 43,
                "x": 879,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bcf590c1-9efb-4fa3-b4a1-62bcf06b0665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 65,
                "offset": 2,
                "shift": 54,
                "w": 49,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "64998212-839a-4a10-834e-0899db6d072e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 65,
                "offset": 3,
                "shift": 54,
                "w": 49,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "33bbc25b-3b82-42c1-ac7f-9bb4d8a17886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 43,
                "x": 789,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "325cdc6e-d7b0-4e48-8dcb-0992288c579c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 355,
                "y": 69
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "57ba30f6-146c-4fae-a791-9ecf0c91f1dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 619,
                "y": 69
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1b9ac0b8-9f3e-4655-80d3-0a0c00581c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 65,
                "offset": 8,
                "shift": 54,
                "w": 40,
                "x": 833,
                "y": 69
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7471b685-534a-44b6-83d3-45d41f1a003c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 65,
                "offset": 10,
                "shift": 54,
                "w": 39,
                "x": 335,
                "y": 136
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5ec31acd-e47c-4200-83b4-335fa7195c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 311,
                "y": 69
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c40eae64-22a1-45dd-8b1d-5efd7c172e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 42,
                "x": 531,
                "y": 69
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1bc650af-6936-419b-80eb-5c1a4f44995d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 65,
                "offset": 11,
                "shift": 54,
                "w": 33,
                "x": 2,
                "y": 203
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1e37a418-3037-4169-8c40-5691dc45e298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 40,
                "x": 959,
                "y": 69
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "74aa5c17-e021-4600-b02e-5ba3a72af8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 42,
                "x": 575,
                "y": 69
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ad9798e1-f5e6-4688-a92b-06e07cb2173b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 65,
                "offset": 9,
                "shift": 54,
                "w": 40,
                "x": 128,
                "y": 136
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dc562723-73c8-47be-8e7d-d98e3c241e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 65,
                "offset": 3,
                "shift": 54,
                "w": 46,
                "x": 413,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "87edd654-5bf1-40fa-ab33-8a98b8c9eca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 223,
                "y": 69
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3dd5d3ba-9a96-44f8-bfca-9e1fa0a7433b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 45,
                "x": 650,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6d16f321-39f7-4dc0-837a-ab7ed807ea57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 65,
                "offset": 11,
                "shift": 54,
                "w": 38,
                "x": 536,
                "y": 136
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eccfae3a-78ec-4181-a204-f05300926554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 48,
                "x": 315,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d4f461a8-af2e-4bb2-b9f5-958aa6b10894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 91,
                "y": 69
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "eb2db43e-2dc3-47a0-942a-f4c06ba181df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 43,
                "x": 924,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "63751b20-e3a0-4a3b-8e62-a0bb9aa180ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 45,
                "x": 697,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "004eba0a-0a52-4967-af6e-2f93685ba5ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 135,
                "y": 69
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c65ea214-66b0-4247-876b-1a1a969dcfd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 65,
                "offset": 1,
                "shift": 54,
                "w": 49,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f8d58ef7-8427-4cc4-8a2f-cb262c0bd054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 65,
                "offset": 3,
                "shift": 54,
                "w": 46,
                "x": 365,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9802dbf0-cbcd-4a19-a87b-aefa93f49ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 42,
                "x": 399,
                "y": 69
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9878a9c1-f089-4dc5-98dd-316dcc61102e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 45,
                "x": 509,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "719979d8-1837-44ea-8839-374122874af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 40,
                "x": 86,
                "y": 136
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "918f0024-0209-465e-9b11-71273d9ff2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 65,
                "offset": 17,
                "shift": 54,
                "w": 21,
                "x": 409,
                "y": 203
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8a31b473-2993-4e2d-9f41-6d0832194356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 42,
                "x": 487,
                "y": 69
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "54e2f2bf-6177-49a5-8419-b2bafba24482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 65,
                "offset": 17,
                "shift": 54,
                "w": 21,
                "x": 386,
                "y": 203
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "12c9a6ce-7205-48fc-bbd4-2452d622b4ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 14,
                "shift": 54,
                "w": 26,
                "x": 491,
                "y": 203
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a34b25b3-9d5a-4209-859f-b43555ece770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 65,
                "offset": 0,
                "shift": 54,
                "w": 1,
                "x": 636,
                "y": 203
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "60164c81-f1d8-4861-a83d-7a98d0a14d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 18,
                "shift": 54,
                "w": 18,
                "x": 616,
                "y": 203
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3e460b68-cc19-413f-84cf-93b451b31299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 43,
                "x": 2,
                "y": 69
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "97323922-63b9-4730-a6fc-31d2eaa3505a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 37,
                "x": 734,
                "y": 136
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "224633a1-c4ae-4736-a164-85231d4755fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 39,
                "x": 294,
                "y": 136
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "82bdf0a3-b400-45a5-8fa0-2068431620cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 40,
                "x": 917,
                "y": 69
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4587fb27-0623-49e4-b960-75997ae09418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 65,
                "offset": 10,
                "shift": 54,
                "w": 38,
                "x": 616,
                "y": 136
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e2b29a0b-c0b9-4536-b220-b16dea334cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 65,
                "offset": 11,
                "shift": 54,
                "w": 37,
                "x": 695,
                "y": 136
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fb247f2f-f2cb-4936-a042-98f3c831a159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 40,
                "x": 791,
                "y": 69
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "13863b92-8b9a-4445-b2ff-0b839e2a115b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 41,
                "x": 663,
                "y": 69
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "78a8e711-f52b-4694-9263-74fb0603fbc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 65,
                "offset": 12,
                "shift": 54,
                "w": 31,
                "x": 73,
                "y": 203
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1213dab6-9d9e-44c2-9ae9-e7e8c14f19cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 38,
                "x": 576,
                "y": 136
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5381d208-82e5-4d1f-9069-47961547b925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 40,
                "x": 44,
                "y": 136
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8d853deb-8eab-40ad-95ea-01c4f79df746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 65,
                "offset": 11,
                "shift": 54,
                "w": 38,
                "x": 456,
                "y": 136
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7f6e793a-ea99-43c0-aff0-f13607d9ef3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 45,
                "x": 556,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c7fa2497-a23e-4f23-b2fb-baac0d647c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 40,
                "x": 875,
                "y": 69
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a230b679-80f5-4d45-a34d-5e094f8ce6e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 42,
                "x": 47,
                "y": 69
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "477dfbb0-0efc-462e-ad91-969dd776af81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 65,
                "offset": 12,
                "shift": 54,
                "w": 35,
                "x": 926,
                "y": 136
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a08991da-e526-4fdc-b18b-d2cd46d3b006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 46,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d645af00-d9f1-460d-8182-387044c1c04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 65,
                "offset": 9,
                "shift": 54,
                "w": 36,
                "x": 888,
                "y": 136
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c27b9003-5156-4e43-be19-875d8092baff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 65,
                "offset": 9,
                "shift": 54,
                "w": 36,
                "x": 850,
                "y": 136
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e40b6486-6591-4fac-a579-e1c8feb24dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 43,
                "x": 834,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3fb84057-5964-4c13-b2e6-45f6acb20b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 40,
                "x": 2,
                "y": 136
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "856d0a66-c56d-4414-925d-957acabbbba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 43,
                "x": 969,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "de24f29a-bc85-4a6e-97a8-4e9d1ee4ce88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 65,
                "offset": 4,
                "shift": 54,
                "w": 45,
                "x": 603,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8448a3b8-a0c3-4293-b9dc-9988ca8b05f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 65,
                "offset": 6,
                "shift": 54,
                "w": 40,
                "x": 749,
                "y": 69
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "55c40e73-0dee-45e9-9355-f98f36ca56cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 65,
                "offset": 5,
                "shift": 54,
                "w": 43,
                "x": 744,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "76f01269-4dd4-4164-a5c1-d895c3c90f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 65,
                "offset": 7,
                "shift": 54,
                "w": 38,
                "x": 416,
                "y": 136
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bbc7e30c-3a22-46db-ac89-61000e7ee0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 65,
                "offset": 13,
                "shift": 54,
                "w": 25,
                "x": 204,
                "y": 203
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b4b890f0-6dee-4bff-b077-209404a854b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 65,
                "offset": 22,
                "shift": 54,
                "w": 9,
                "x": 594,
                "y": 203
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e5aa3703-7481-4397-8809-d39ed108a27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 65,
                "offset": 13,
                "shift": 54,
                "w": 25,
                "x": 231,
                "y": 203
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e8b88906-c519-45c0-a2cc-937c377461b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 64,
                "offset": 13,
                "shift": 53,
                "w": 25,
                "x": 285,
                "y": 203
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "b5eab175-eb3a-44bc-a46b-e76b991f879f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 64,
                "offset": 13,
                "shift": 53,
                "w": 25,
                "x": 258,
                "y": 203
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}