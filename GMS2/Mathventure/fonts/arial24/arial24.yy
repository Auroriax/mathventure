{
    "id": "7c56e05e-9ac8-4fd8-adbc-77c02bd92056",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "arial24",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d19a337d-0322-45af-af90-8c115f0f8d8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 422,
                "y": 81
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "86f6c36f-9192-4251-ab37-15e17b0468ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 112,
                "y": 121
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a994e89c-aff5-4970-b2c9-f0c99ee28a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 63,
                "y": 121
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "98b10a76-395f-405e-8551-6c6c7c34ea27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 301,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "323c30fc-211d-4cd7-a5a5-97e0be182d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 283,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a81219b2-d4a8-4ab2-992b-e992a781176b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fdd5c0b5-67de-47f6-9332-396acddd23b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d9b32765-04ff-4361-aefb-52aeb93422f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 148,
                "y": 121
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "91f1062b-9533-40cc-be2e-fa7c1c652ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 501,
                "y": 81
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "117ce127-0c12-4403-82e9-e1700541205a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 481,
                "y": 81
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7569dddd-da94-4a89-a7b0-964a0f23991e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 50,
                "y": 121
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "827ca879-1df0-47d8-9400-3aaede0e6085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 27,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 296,
                "y": 81
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5d4f239f-8a5f-4f01-8849-693b8c5260ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 84,
                "y": 121
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0da92c33-6a4f-4309-9d91-0a647e87fc86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 39,
                "y": 121
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "143f506b-f7f3-46e0-9268-9186aa5ec5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 119,
                "y": 121
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "93a76a13-6548-41a6-8835-192a7135e251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 433,
                "y": 81
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "24be6d85-c89e-435d-aab0-dfe49eea6947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 124,
                "y": 81
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "12301603-222a-4473-8f2f-11324788e3d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 469,
                "y": 81
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "be00591b-e920-4513-8d1f-833efcbe9801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 192,
                "y": 81
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6563709f-5766-4c60-a4b1-55a1751f67ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 141,
                "y": 81
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e592c463-dfe6-4f65-bd04-7a4cf4226fc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 339,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cd576606-ce96-4250-8f5b-917fd1d14307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 81
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ec0634b3-8581-46b8-a7da-45ddab51e00c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 90,
                "y": 81
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "227d172f-4c73-4b28-a606-ad48969968d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 175,
                "y": 81
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cdc72d57-b5ea-4587-b0e6-c56d0fcfe610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 158,
                "y": 81
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2d32f583-d4f9-412c-aefe-be5a454c0ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 56,
                "y": 81
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6f445960-e138-45f8-86f2-eedea931e217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 91,
                "y": 121
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "15eb8ac4-3aa5-4100-a907-3be044c2ccaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 77,
                "y": 121
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1e1a509c-65b7-432c-be5b-66291ab1d4cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 260,
                "y": 81
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0259484e-4548-4637-8672-a2fc519a1dce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 340,
                "y": 81
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "778a2f65-a198-450c-bf7b-7683c081cc10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 278,
                "y": 81
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "57fa13b0-da21-4c24-93ec-f7d63261a1f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 377,
                "y": 42
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "65f6ed93-89fb-4d19-9993-17b32b0a3dc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4628eaa7-dfd7-4385-b367-f7c214cbb7ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "72807028-d7a7-4022-b00c-892aeaaab041",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 122,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "583f6a3d-52a1-4d5e-a774-ecd3fbab0495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "65a880f8-926c-464c-8888-f56acaa2d830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 59,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "25996717-3952-4c77-b76f-d3534ba5ad6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 183,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5bb50e43-9fb3-4753-8da2-d30db729c7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 432,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "15ca8aa0-5b5e-4be3-8ce7-299fe4339fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "07da8037-1adc-4055-975d-f1e2b9180e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 163,
                "y": 42
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b1227b07-a288-4da3-a4e1-52ac6b741ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 126,
                "y": 121
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "277ba517-4a84-427c-97fb-20f9e0c4ce27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 107,
                "y": 81
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6012dc23-e039-4794-8bc2-651548703e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 21,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "49dee545-984f-43ee-a6fe-4ad9c6278fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "666be581-b325-4a3f-b555-c499f86c222b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6e8745be-2a85-43b8-8c7b-0f0614c9db34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 263,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "87a88326-a3a1-4edf-9cf9-4bc1d679b0ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "21836d86-0d0d-4dc8-b266-f5b9f2838b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 243,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b98ba51a-12d9-4881-a8f7-1d068b52ae6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "45ccc028-52d9-4c55-879b-31b7ed1059df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 21,
                "x": 389,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d5f6f0ca-72d9-482a-89d5-5f34fc1385d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 80,
                "y": 42
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b34f597c-5210-4898-bd93-da62e9b28f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1b0ff5c5-7d94-4e1a-b4d9-49a2fb41a209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 143,
                "y": 42
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d7944ea6-eba2-486d-a2c3-4c9b2037ae7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e6cc3bbf-3346-47af-b691-9805d69d9dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9f075873-5bcf-40c5-a089-4fec18caf2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7db8a932-98dd-4410-b532-8ba2b0d19be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4f7655aa-1184-4884-9667-2904a0929559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 38,
                "y": 42
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "dc65e1da-9121-4fdd-8ac9-b9595af8d384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 12,
                "y": 121
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "230c0590-ac82-4ca0-8e87-2179a593986a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 445,
                "y": 81
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6da859b0-01cd-4181-8af6-2a64b6add203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 491,
                "y": 81
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0a0c0a2d-8e94-4c3b-96af-17c6965b197d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 22,
                "y": 121
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2b5c2101-0eda-43dd-8aca-c5b7c6ddd557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f20fcdb3-18ad-49cf-a05e-0f08cdeeebe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 139,
                "y": 121
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5929d22c-f74b-4490-aa8a-d7d8934b7647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 486,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6199f65b-38e2-4ee5-b826-b5a715620695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 396,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "33ca903b-477a-4958-90aa-55da0388d8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 450,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0b382721-e5fa-4ef7-a368-71062832027c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 414,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "489a5c10-b257-4cd4-abbc-9c60d446106a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 358,
                "y": 42
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b852b5d0-74b2-467a-8c88-5ade16813146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 396,
                "y": 81
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c7b9a9bb-bfa3-4b90-b622-ff097ec814e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a3572075-1059-4ace-aad0-4359cf7099a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 226,
                "y": 81
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "00895acd-91e7-4132-b56d-81f3c92294ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 105,
                "y": 121
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2a16a3a4-7c8d-4c11-a75f-0cb2c6f4d2dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 121
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "327a7149-aadb-4beb-a51c-97b7acf54fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 81
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9d903ce0-3f5f-412a-af15-662f16031cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 98,
                "y": 121
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "feb7d7f3-1023-4cf3-a0f2-aa032aa67613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "86bed43a-94a1-4720-b303-d943c8e117da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 209,
                "y": 81
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "73687128-ef97-41c5-bda6-a95b81dd87b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 320,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bb06720d-3106-4975-94ca-660f770d703b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6efee348-b202-40b9-8c00-c98d5b12bb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 20,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f988ad6e-4fb9-4654-b982-4ce487ee768e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 3,
                "shift": 13,
                "w": 11,
                "x": 409,
                "y": 81
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a3ca4bc1-da90-4b6b-b4e4-d8902d5e84de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "392a6bbc-5c21-4a8b-aee0-c72144987056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 457,
                "y": 81
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "321eaa4a-a577-42c1-9127-83e7c21e66bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 73,
                "y": 81
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "88c64496-24a7-4f7b-85c2-05851550a377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 223,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "60cbf19c-ae25-4130-b9e9-cb4732b57995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3b18475f-1bc6-4c10-b47b-e5e392480e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 203,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9ef3a5cc-5efd-43f5-a573-07b66336e3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b0641e7e-7cbb-4777-8352-0bd53056c96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 243,
                "y": 81
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "19ff871c-33b2-4762-9b94-774163cd3721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 327,
                "y": 81
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "87948afc-ea63-49b6-9c26-90fc7523f67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 133,
                "y": 121
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ad45d406-f6ce-4993-a2f6-3861ee10b6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 314,
                "y": 81
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8e593ed7-bd44-4c18-b345-112b6c41d061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 358,
                "y": 81
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "2fdf7f86-cf5e-4323-b156-bb168e708dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 377,
                "y": 81
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}