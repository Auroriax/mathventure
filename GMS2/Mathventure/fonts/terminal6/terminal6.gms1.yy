{
    "id": "4b390f90-5cb4-4790-b1d7-85c1ccfdb55f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "terminal6",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "SimSun",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7a8b5958-1138-42d0-9b40-00f37261b2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6a7bb6bd-0ce7-4cc3-a77f-523616fae0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 2,
                "shift": 6,
                "w": 1,
                "x": 124,
                "y": 56
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8cf3c182-4902-46a8-b848-190de56ca6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 3,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 109,
                "y": 56
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e333f2eb-ea54-4228-adf7-1ce208dde8bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5efe18bf-baa2-41be-afd3-bb17e1e8d708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 16
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "224ec57a-b04a-44c7-992e-a794cec5e8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "426f8994-bbb5-473d-8b7a-c9ff7995aaa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "eb942692-78f4-4ec8-929a-ab3957eeab1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 3,
                "offset": 0,
                "shift": 6,
                "w": 2,
                "x": 17,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9cd8dda6-5116-4ce8-897c-45c105d77cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 85,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5773e519-48c7-4a84-9bad-ff4744c7cb2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 80,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4842cf37-7951-43ae-b3e7-0f00a4d59cd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 99,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8a87977a-ee9d-4f24-924c-48be9fa26590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 8,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9f287f74-e365-4e36-bf25-ac4bca52297d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 2,
                "x": 105,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b1b829e1-0f01-4034-b8f6-375bf05d0c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 6,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4ad761d8-2b41-4b11-9e3d-11840a9355db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 1,
                "x": 9,
                "y": 70
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "13dd9602-35e6-4005-ade8-a163020d2fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1728b26c-1389-4cb7-a40b-3c14329887c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "36bf7504-f9db-454f-bcb6-5ad91b71acd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e1d44bad-ccae-4db1-b39d-fe5a9799b3c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d7bbdd4e-6efe-44c4-8d32-64fa513e4812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 16
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "353c0036-3a50-41de-a5b0-6b578dc863f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4c58a943-4d41-4102-8182-7592377c02a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "70c3f94d-d0ec-49e6-9a28-5c270dc4e126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 30
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "657c41bf-9d5d-4422-86d3-360c50b93cd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 21,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "564f0041-6ac3-4c89-a7c0-94ed5a151c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 30
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "97420ee9-d6b4-424d-bae6-219ba83a662b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "48b50b2f-a303-4250-81e3-ef9c3ad5d5d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 2,
                "shift": 6,
                "w": 1,
                "x": 121,
                "y": 56
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b3b8fcc8-410c-4914-83de-551aead823e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 1,
                "x": 118,
                "y": 56
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7882bb53-a5af-447f-9c90-7a927ab0aca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 39,
                "y": 56
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ecce5ce1-6e5a-4ada-a45d-4f4c06d3392c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b77d28f4-b82c-4262-a749-a66099a00d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a5f87445-1178-4a59-8db0-a3a936d4d9ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 42
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "87002a9e-ec77-4b67-8237-b1cfe0863cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "05b5db2a-275a-491a-9fd4-d7ce6d451e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 16
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c620f080-f3b8-4241-9a5e-8fd82edd3aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1750e7a5-468b-478d-8c05-3ab448d32b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 16
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a2bf9d70-cb36-4dd4-b042-6ed0138f9649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "029bccc6-e1f2-425d-8f8c-4d3fccce2dc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a59ed544-1a5d-4602-8ae4-a6852bf92524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 30
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b3b311be-8e11-4208-81e4-411e6f1ede83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f0ead3f0-4141-4d15-ad5b-179adb851ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 16
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ceb94512-9fea-4b87-9780-3efc2c8a36ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 30
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "097cb7b2-170c-4b59-be1b-a9a6d2de7677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e7c76b67-d1b2-4af6-a39e-29235b48934d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 16
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c19bfe19-7546-4d25-97b3-dfa106d93c98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d2f59f58-d3de-4559-8cd3-851df611beb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c551f3b8-48e7-4754-9fa3-00eb7dc201aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 49,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bb1e4547-6b30-4da7-b664-cff12db74fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 30
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f187b278-f3ad-4d29-8042-1e7c0c1983e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 16
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f787636b-26d7-4076-98a0-b7420a57e782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e5b2da05-1776-4cb9-899e-1044914d496f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cb661b56-b923-4b45-9689-ab296ac0424b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 42
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "19962506-c828-44d2-aa9a-a30fdce661f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 16
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "afd4037a-7950-498c-a530-c46b738a7fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cff272bf-3e72-4505-87d4-1f65e4b94a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "58c56359-80a8-46ed-a10e-934fb5f3f4ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 30
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ac17ff7f-aff7-4fd5-914f-705782709297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 30
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e524f4b0-7126-4f95-825d-2600ad789b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 42
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b2867293-2581-423d-b36d-ba329866ee48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 42
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1fd3c653-c8a0-4bcd-b453-5ee624060bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 75,
                "y": 56
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e0456e97-06b3-412a-8bae-134b789f26d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 106,
                "y": 42
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e0c71be2-58fa-4b6c-825e-1a5ebaa1242e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 65,
                "y": 56
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c08eddcb-eb3d-4efd-9521-eacb514ca187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 2,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 12,
                "y": 70
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "eb42d127-441c-4d62-8450-570fb72177d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "15bbd401-18b1-42e4-9f5e-18e56f510feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 2,
                "offset": 1,
                "shift": 6,
                "w": 2,
                "x": 21,
                "y": 70
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d333c4c2-2de0-4f0b-84cb-0ea03119443f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a12c7047-6ae0-4c9f-b149-f2aa90e844e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6f5af647-8c5e-4475-8287-808a72ca0c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 27,
                "y": 56
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "daddf003-130e-44d2-a110-359b11390362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f7b558f6-a6e8-4104-8b78-47a8b5d13caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 51,
                "y": 56
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "856dd762-d9ff-4e66-91ae-e9e6ac775a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c10302d5-1ab3-4eb9-825a-701c0971d1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 16
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d7eed138-55ea-4310-99ce-e694196bdf7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1f2c6776-ca91-4851-8f79-a947980a0098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 95,
                "y": 56
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "552767c6-364d-46dc-ae74-1876e6eb4676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 93,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9cbab9ce-f63a-4f4a-a773-90f46f7e933e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "61639e78-836a-4723-ba8b-698a2ceb17f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 30
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e11fcb75-fe05-4a07-8b8b-470c906e7e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 30
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a04cd6d7-7fac-40ea-88eb-5463a0bf928d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 16
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5b83da58-2b86-416b-a9f0-ec84060c78aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 33,
                "y": 56
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "509c4fc6-8ba4-495d-a75d-6b76a8582387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ebab8bec-6f61-4021-bfff-1552deb1beae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 16
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3d09b4d7-2b41-48ab-a8c7-a4e8cacef3c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "60320691-74ce-4449-a170-0ecba345262d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7b226050-0387-4880-9580-c5eb27c7aeb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5b970ae6-cce9-4483-b0d4-c3de6cd025da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "83a088a2-53f8-47ae-b1a4-0dd84ccb6e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 16
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2ef707ad-0fef-4d12-a909-192d77cabd69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "716735a9-d8eb-4dad-8f2c-fe3dd476fdf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bcdc21cf-4eaf-4a1a-ba9d-05fd6248509d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f7ebad65-5900-46e7-88b3-ad56b00b9f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 45,
                "y": 56
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4d4a19c3-60fa-4188-ab82-c069a890ea5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 90,
                "y": 56
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d484e4e8-ec45-402e-a20d-23ed2a8e23b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 3,
                "shift": 6,
                "w": 1,
                "x": 115,
                "y": 56
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2daa7396-8342-4388-be70-fc13fcd2dd2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 70,
                "y": 56
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8124261c-0694-41df-b05f-850e6dbde7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 2,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "0ae2d3b8-19ea-4ad0-bc77-8ddd64e3bdac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 0,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 25,
                "y": 70
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 9,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}