{
    "id": "dd4ee24f-08d7-447c-88fe-2c3af74febd4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_credits",
    "eventList": [
        {
            "id": "eb94a7e0-715e-4b41-af8e-feca25825dc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd4ee24f-08d7-447c-88fe-2c3af74febd4"
        },
        {
            "id": "31fed575-cea9-4701-a961-6bcd21c6c945",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd4ee24f-08d7-447c-88fe-2c3af74febd4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "079ba903-5192-461f-bc6f-2955a9d782b7",
    "visible": true
}