draw_set_font(terminal12); draw_set_color(c_white);
scr_credits(40,30,"Mathventure Credits",
"Game: Tom Hermans",
"Logo Font: Taurus Mono Outline by Tyler Finck under SIL Open Font Licence",
"Music: Cipher by Kevin MacLeod under Creative Commons By-Attribution 3.0",
"Thank you for playing!#(C) Tom Hermans 2013, 2016, 2019")
draw_set_valign(0)

