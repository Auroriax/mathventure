{
    "id": "f1b7b3e2-fbce-46ec-90d4-e7688d807a2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_wallsp",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "383e757d-003f-4290-a476-5b06c63a7a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "acb19995-868f-4150-954e-efee2418297a",
    "visible": true
}