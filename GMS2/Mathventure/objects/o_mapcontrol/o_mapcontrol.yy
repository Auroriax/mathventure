{
    "id": "e29e2089-cf3f-44f7-b4d3-a90677db56e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_mapcontrol",
    "eventList": [
        {
            "id": "270144c4-4401-4978-a79a-6f3bbf1ef8d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e29e2089-cf3f-44f7-b4d3-a90677db56e4"
        },
        {
            "id": "5a5e58fb-19c8-4b1c-853c-173caa26d731",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 19,
            "eventtype": 6,
            "m_owner": "e29e2089-cf3f-44f7-b4d3-a90677db56e4"
        },
        {
            "id": "6b3f8849-2bd7-49ab-bcdb-c58e2c954e13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 6,
            "m_owner": "e29e2089-cf3f-44f7-b4d3-a90677db56e4"
        },
        {
            "id": "b7c2f163-8d49-485a-b841-448388489eea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 6,
            "m_owner": "e29e2089-cf3f-44f7-b4d3-a90677db56e4"
        },
        {
            "id": "61df5d0e-e1c1-4672-858c-f984666874bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 6,
            "m_owner": "e29e2089-cf3f-44f7-b4d3-a90677db56e4"
        },
        {
            "id": "c646d040-75fb-4d71-b389-24436166a443",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 123,
            "eventtype": 9,
            "m_owner": "e29e2089-cf3f-44f7-b4d3-a90677db56e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}