{
    "id": "3c3f4b27-6916-4831-b047-86087e39d30f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_multiplesqempty",
    "eventList": [
        {
            "id": "40002ce5-5449-4ab0-99ac-0eb3d01e8763",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3c3f4b27-6916-4831-b047-86087e39d30f"
        },
        {
            "id": "7fe4b8d6-2dc0-44b3-885f-e9fee1c461f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3c3f4b27-6916-4831-b047-86087e39d30f"
        },
        {
            "id": "ab8d2a14-0286-42a4-a64e-0c495853c4d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3c3f4b27-6916-4831-b047-86087e39d30f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e424a7ed-c561-4859-be94-575c7a23ec2b",
    "visible": true
}