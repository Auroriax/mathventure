{
    "id": "31ffdabc-aff3-423f-9739-f92636ccadb4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_minusrdempty",
    "eventList": [
        {
            "id": "3c6b7aa2-93ef-4534-b92c-42e6b3416151",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "31ffdabc-aff3-423f-9739-f92636ccadb4"
        },
        {
            "id": "5f1341dc-d912-48e5-a89a-cecdcde7a530",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "31ffdabc-aff3-423f-9739-f92636ccadb4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bbe79cf-66e5-4c7c-aec1-0b8c9569b763",
    "visible": true
}