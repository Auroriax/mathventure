{
    "id": "e375a3b9-3eeb-489a-b8a8-8a2c3f5bab83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_subtractrdempty",
    "eventList": [
        {
            "id": "be2c5f97-7957-4c50-a014-6d8bb35353f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e375a3b9-3eeb-489a-b8a8-8a2c3f5bab83"
        },
        {
            "id": "017a11ca-40b0-48ab-9631-f666a1e2ed19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e375a3b9-3eeb-489a-b8a8-8a2c3f5bab83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a38f5fed-de68-4329-ad51-8dd48f191603",
    "visible": true
}