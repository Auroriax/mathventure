{
    "id": "625f047c-547a-49e9-88b0-b66566f48b4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_completion",
    "eventList": [
        {
            "id": "9171e2ff-c755-42cf-93ed-23fc1769576f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "625f047c-547a-49e9-88b0-b66566f48b4f"
        },
        {
            "id": "2efe9505-5378-4ef9-a4ab-4e0198722831",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "625f047c-547a-49e9-88b0-b66566f48b4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "852aff7c-dfaf-455f-8362-35ce04120660",
    "visible": true
}