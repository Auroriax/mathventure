{
    "id": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hero",
    "eventList": [
        {
            "id": "b24e8dc5-edfd-418d-b3ef-e29c0e732d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "f846b615-f372-4eeb-b44d-28e6ac37a7bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "dd7ee111-e73a-47f6-984f-aa4e95a3df2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "a11a0155-cf4f-48b6-8434-99542cf2d2f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4f70f6ee-2251-49e5-b537-766a43b3ceda",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "51a94167-6bee-4154-a9bf-fb9fee8eb2b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bd184756-4579-47c1-ba13-8618dce9ffe6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "d4715b9e-4c9b-45d5-81d8-19251c344821",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "77fbd363-fb00-49c8-9d15-bc97087d61a9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "819154db-1e9b-4f88-b472-c44c8e89079a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f08f500e-6389-4451-a4a0-f42df4d7b05d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "4ee76812-e290-4768-aa4d-ad7e7a210875",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "383e757d-003f-4290-a476-5b06c63a7a26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "676293bc-c7ad-4b8f-90ba-5f41f1c20971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "5301a59d-5f5c-4852-9ebf-772697d693cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "e3ddc036-0f53-4728-b976-ceb3649de5cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "2661e1ca-93fb-4796-a2d5-b4862259bd7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        },
        {
            "id": "3cf2c9d7-b2fd-4184-a600-f26d195d6bdf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 9,
            "m_owner": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "508f8797-c1b0-445e-b714-be7a7bfb5e85",
    "visible": true
}