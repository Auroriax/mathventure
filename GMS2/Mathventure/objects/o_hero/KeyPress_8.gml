if undoc > 0 && speed == 0 && place_snapped(24,24) //Can only undo when not moving & program contains undo states
{
//show_debug_message("Rolled back to state "+string(undoc-1))

undoc -= 1

with o_trapswitch
{
 use = false
}

with o_trapactive
{
 if turnactive == other.undoc
 {instance_change(o_trapswitch,true); turnactive = -1}
}

x = undo[undoc,0]
y = undo[undoc,1]
global.number = undo[undoc,2]

with o_modifierparent
 {
  if turnused == other.undoc
   {x -= room_width; turnused = -1}
 }
 
var cont;
cont = instance_place(x,y,o_modifierparent)
if cont > 0
 {
  cont.use = true;
 }
}

