{
    "id": "635b3534-e679-4feb-b5c9-5937946a21c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_unrelevant",
    "eventList": [
        {
            "id": "3040feea-3181-48eb-952c-8f531a430678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "635b3534-e679-4feb-b5c9-5937946a21c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "488e8ed4-087b-474f-b5f8-2a36b0cc2c0f",
    "visible": false
}