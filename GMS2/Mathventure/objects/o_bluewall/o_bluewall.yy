{
    "id": "771903ba-bb26-401d-9835-4daca802cff3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bluewall",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "383e757d-003f-4290-a476-5b06c63a7a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "488e8ed4-087b-474f-b5f8-2a36b0cc2c0f",
    "visible": true
}