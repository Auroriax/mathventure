var __b__;
__b__ = action_if_variable(room, r_map, 0);
if !__b__
{
{
var offy = -2;
if global.number < 0 || global.number > 1000000 //Game over
{
    draw_set_alpha(0.75); draw_set_color(c_black)
    draw_rectangle(0, room_height/2 - 60, room_width, room_height/2 + 60,false)
    draw_set_alpha(1);
    offy = -room_height/2 + 50;
    
    draw_set_font(arial24); draw_set_halign(1); 
    var txt = "Number went below zero."
    if global.number > 1000000 {txt = "Number became higher than one million."}
    draw_text_outline(room_width/2,room_height/2-50,txt,c_black,400)
    draw_set_colour(c_white);
    draw_text_ext(room_width/2,room_height/2-50,string_hash_to_newline(txt),-1,400)
}

//Target text
draw_set_font(terminal6); draw_set_halign(2); 
draw_text_outline(room_width-2,room_height-42+offy,"Target: "+string(o_goal.finish),c_black)
draw_set_colour(c_white)
draw_text(room_width-2,room_height-42+offy,string_hash_to_newline("Target: "+string(o_goal.finish)))
draw_set_halign(0);

//Current value text
draw_set_font(arial24)

if (global.number == o_goal.finish)
{
    draw_set_colour(c_lime)
}
else if (global.number < 0 || global.number > 1000000)
{
    draw_set_colour(c_red)
}
else {draw_set_colour(c_white);}

draw_set_halign(2)
draw_text_outline(622,588+offy,string(global.number),c_black)
draw_text(622,588+offy,string_hash_to_newline(string(global.number)))

//text
draw_sprite(s_deathdialog,0,4,623+offy);

}
}
