{
    "id": "2fc20477-db5b-49f2-92d8-161120341232",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_control",
    "eventList": [
        {
            "id": "aa7e8853-5367-4f68-be47-8c821466fd12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        },
        {
            "id": "ebb568b3-2c1b-4898-a103-fe673aa6d4f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        },
        {
            "id": "6b513bf0-3f65-4cea-b9a2-c3ab736f3f15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        },
        {
            "id": "fdb613f8-01f3-4f3e-8f29-9b04e89cb666",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        },
        {
            "id": "205178e6-4dc7-4e39-b643-235dd1927bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        },
        {
            "id": "6b1de2d4-4436-4505-b3b5-1a0a5d64e47d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 55,
            "eventtype": 9,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        },
        {
            "id": "b6220175-926d-4f1a-88b5-c0bd1d1c31c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "2fc20477-db5b-49f2-92d8-161120341232"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}