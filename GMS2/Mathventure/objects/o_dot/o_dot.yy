{
    "id": "e3d91bff-2023-4863-abde-7c1d1e045436",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dot",
    "eventList": [
        {
            "id": "794973ef-b5d1-4baa-b784-fe6af0d74d90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3d91bff-2023-4863-abde-7c1d1e045436"
        },
        {
            "id": "dd05cfb1-371c-46f2-926b-870dcb420ab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e3d91bff-2023-4863-abde-7c1d1e045436"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3c2bbd11-a3b6-46b6-ab9d-ef228dd43200",
    "visible": true
}