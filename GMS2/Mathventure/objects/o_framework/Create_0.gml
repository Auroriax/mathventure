keyboard_set_map(ord("A"),vk_left)
keyboard_set_map(ord("W"),vk_up)
keyboard_set_map(ord("D"),vk_right)
keyboard_set_map(ord("S"),vk_down)
keyboard_set_map(vk_enter,vk_space)
// Azerty support
keyboard_set_map(ord("Z"),vk_backspace)

global.sfx = true;

//set_application_title("Mathventure")
ini_open("MathventureSaveData.ini")
if ini_section_exists("Warning!")
{}
else
{ ini_write_real("Levels","tut1",0)
ini_write_real("Levels","easy1",0)
ini_write_real("Levels","easy2",0)
ini_write_real("Levels","easy3",0)
ini_write_real("Levels","easy4",0)
ini_write_real("Levels","easy5",0)
ini_write_real("Levels","easy6",0)
ini_write_real("Levels","medium1",0)
ini_write_real("Levels","medium2",0)
ini_write_real("Levels","medium3",0)
ini_write_real("Levels","medium4",0)
ini_write_real("Levels","medium5",0)
ini_write_real("Levels","medium6",0)
ini_write_real("Levels","hard1",0)
ini_write_real("Levels","hard2",0)
ini_write_real("Levels","hard3",0)
ini_write_real("Levels","hard4",0)
ini_write_real("Levels","hard5",0)
ini_write_real("Levels","hard6",0)
ini_write_real("Levels","secret",0)
ini_write_real("Levels","total",0)
ini_write_string("Warning!","This is save data for Mathventure","Changing or deleting it would cause data loss in the game")
ini_close()
}

if os_browser != browser_not_a_browser
{

}
else
{
    alarm[0] = 3;
}

globalvar music;
music = true
sound_global_volume(1)

gpu_set_alphatestenable(true);