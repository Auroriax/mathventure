{
    "id": "b0df36c8-cac8-44d4-88c7-b5a177a1643e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_framework",
    "eventList": [
        {
            "id": "e866accf-7b2d-4ba2-87ea-66c55dbf9c3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0df36c8-cac8-44d4-88c7-b5a177a1643e"
        },
        {
            "id": "013d110a-2e1d-4db3-b342-67a02770db01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b0df36c8-cac8-44d4-88c7-b5a177a1643e"
        },
        {
            "id": "2a91ee3a-105c-43b6-94ef-aa58bfa16932",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "b0df36c8-cac8-44d4-88c7-b5a177a1643e"
        },
        {
            "id": "8b0438dc-8479-42c9-8077-b18ff6ecec0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b0df36c8-cac8-44d4-88c7-b5a177a1643e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}