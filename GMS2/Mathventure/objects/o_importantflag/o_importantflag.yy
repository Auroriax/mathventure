{
    "id": "b94ac1f1-34ae-406d-865e-e73601d6387c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_importantflag",
    "eventList": [
        {
            "id": "032163d2-231f-44cb-96a0-6696c4dc537a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b94ac1f1-34ae-406d-865e-e73601d6387c"
        },
        {
            "id": "9b8e2046-b1a5-4d5d-9b2a-02bd6d2c03dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b94ac1f1-34ae-406d-865e-e73601d6387c"
        },
        {
            "id": "99cf958c-73bf-4231-a57f-878a14809a7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b94ac1f1-34ae-406d-865e-e73601d6387c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f508cd01-5212-4a0f-af71-f08c7d63c63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1328c1f7-615e-45d7-88f7-96eb8adeb27e",
    "visible": true
}