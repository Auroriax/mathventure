{
    "id": "9a5fc4af-e49e-46d2-a940-ec87a0f26179",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_splash",
    "eventList": [
        {
            "id": "54366217-5761-4820-ab8c-3856a9617c00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9a5fc4af-e49e-46d2-a940-ec87a0f26179"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d39c2577-157e-4143-9de1-19787de39a07",
    "visible": true
}