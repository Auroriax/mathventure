if os_browser == browser_ie
{
	show_message("Audio is not supported in this browser. Please upgrade or change your browser.")
}
else if audio_system_is_available() == false
{
	show_message("We don't have permission from your browser to play audio. Please check your preferences.")
}
else
{
	if music == true
	{music = false; audio_stop_sound(cipher)}
	else
	{music = true; audio_play_sound(cipher,0,true)}
}
