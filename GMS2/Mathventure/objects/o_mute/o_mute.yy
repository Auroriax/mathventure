{
    "id": "3e8440d8-35c6-4826-8d3a-2e87932f2ed8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_mute",
    "eventList": [
        {
            "id": "20ebaee2-3880-4096-88ba-a1a0afdc899a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e8440d8-35c6-4826-8d3a-2e87932f2ed8"
        },
        {
            "id": "01ad635b-7ae6-46d3-bf82-321295bb32d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3e8440d8-35c6-4826-8d3a-2e87932f2ed8"
        },
        {
            "id": "0c8d4e06-567c-42b1-9964-c877339e3e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3e8440d8-35c6-4826-8d3a-2e87932f2ed8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9ebc9a2a-2401-4e4a-b286-129dcc64647f",
    "visible": true
}