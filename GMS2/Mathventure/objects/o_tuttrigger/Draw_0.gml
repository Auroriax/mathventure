draw_self()

var p;

switch (pudding)
{
case 1: p = "Welcome to Mathventure!#Walk around with WASD or the arrow keys. Other important keys are listed in the bottom of the screen."; break;
case 2: p = "These 'modifiers' you collect, feed into your score, displayed in the bottom of the screen."; break;
case 3: p = "ROUND modifiers can only be used once. SQUARE modifiers can be used multiple times."; break;
case 4: p = "Red MINUS modifiers reduce your score."; break;
case 5: p = "Try to avoid your SCORE becoming negative, or becoming more than a million. If that happens, it's GAME OVER."; break;
case 6: p = "Yellow MULTIPLICATION modifiers multiply your score by their value. Be careful, using these too much will easily cause your score to exceed a million points."; break;
case 7: p = "Green DIVIDE modifiers divide your score. After dividing, the resulting value is ROUNDED DOWN to a whole number."; break;
case 8: p = "Exclamation marked TRAP TILES become solid after you step on them. Once you enter, you can never go back..."; break;
case 9: p = "The goal of the game is to get the the FINISH with the exact target score listed on it. So here's a little puzzle to get you warmed up!"
}

draw_set_halign(1); draw_set_font(terminal12)
if o_hero.x == x && o_hero.y == y
{
var xx, yy;
xx = x+16
yy = y+32
if xx <= 150 {xx = 150}
if xx >= room_width-150 {xx = room_width-150}

draw_set_alpha(0.8)
draw_roundrect_colour(xx-150,y+32,xx+150,yy+string_height_ext(string_hash_to_newline(p),-1,300),c_black,c_black,false)
draw_set_alpha(1)
draw_text_ext(xx,yy,string_hash_to_newline(p),-1,300)
}

draw_set_halign(0);

