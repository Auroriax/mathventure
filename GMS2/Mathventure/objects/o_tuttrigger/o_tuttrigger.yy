{
    "id": "eb452400-32f4-4493-b9e5-d10f37c23769",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tuttrigger",
    "eventList": [
        {
            "id": "fd27a5e3-3bd1-46f3-8f43-3d76ca8c169e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "eb452400-32f4-4493-b9e5-d10f37c23769"
        },
        {
            "id": "8c0734cd-3d70-41da-a5a1-6182d243701a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eb452400-32f4-4493-b9e5-d10f37c23769"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0eae26cf-22fc-4002-b757-22816261a6bd",
    "visible": true
}