{
    "id": "38cb077b-67ef-44d8-90c5-46a69d839a17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_trapactive",
    "eventList": [
        {
            "id": "05fc1f17-0af7-434f-9502-cf3297f06779",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38cb077b-67ef-44d8-90c5-46a69d839a17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "383e757d-003f-4290-a476-5b06c63a7a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "68ae4f7c-976c-47a2-b392-3fe714867d04",
    "visible": true
}