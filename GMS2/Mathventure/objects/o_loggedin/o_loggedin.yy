{
    "id": "4a9f5ae5-d00c-4224-8156-3adc99a8c2df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_loggedin",
    "eventList": [
        {
            "id": "84199526-5c9a-483b-bc29-eee7390413ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a9f5ae5-d00c-4224-8156-3adc99a8c2df"
        },
        {
            "id": "8a24d07e-51cd-4bad-ac54-08554fd97270",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4a9f5ae5-d00c-4224-8156-3adc99a8c2df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8402686d-0034-4977-9ac8-4568ea262c97",
    "visible": true
}