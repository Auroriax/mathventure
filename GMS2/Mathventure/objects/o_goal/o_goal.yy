{
    "id": "009f4418-68e5-46bf-9e20-8940367f8962",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_goal",
    "eventList": [
        {
            "id": "2332e1ab-e2aa-43f1-a6f3-8d697f9b2add",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "009f4418-68e5-46bf-9e20-8940367f8962"
        },
        {
            "id": "f14a7412-1472-43c4-89ac-ad0c91de864c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "009f4418-68e5-46bf-9e20-8940367f8962"
        },
        {
            "id": "028025bc-3077-4299-adf4-5c55c8ede734",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "009f4418-68e5-46bf-9e20-8940367f8962"
        },
        {
            "id": "c5c12abd-fd41-4247-9ad7-db4229e9b73f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "009f4418-68e5-46bf-9e20-8940367f8962"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5fcad098-27e5-4f9c-97ff-b7c19e7c18d6",
    "visible": true
}