{
    "id": "89873be3-9e3e-43e6-bc82-4691ec3a5f30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_flagpulsate",
    "eventList": [
        {
            "id": "beb02dec-00fb-4ec5-a929-51c9df43da94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89873be3-9e3e-43e6-bc82-4691ec3a5f30"
        },
        {
            "id": "8edea800-f31b-452a-bbf3-7288f8b6992a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "89873be3-9e3e-43e6-bc82-4691ec3a5f30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba70b653-fa08-4ec9-8e19-35c1deb97763",
    "visible": true
}