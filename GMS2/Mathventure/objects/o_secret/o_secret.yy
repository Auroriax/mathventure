{
    "id": "260d2b75-88da-425b-83b1-850fb62974da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_secret",
    "eventList": [
        {
            "id": "5aff319b-c936-4543-9162-f87018ce635c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "260d2b75-88da-425b-83b1-850fb62974da"
        },
        {
            "id": "c07e2da8-4521-4c1a-b3a2-362212ba87d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "260d2b75-88da-425b-83b1-850fb62974da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d654916-7019-4251-909d-7cec0026ac83",
    "visible": false
}