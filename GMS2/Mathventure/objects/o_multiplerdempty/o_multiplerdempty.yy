{
    "id": "2dc9dc17-753f-4477-a3d3-7946f3a9c7b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_multiplerdempty",
    "eventList": [
        {
            "id": "c2d990d1-07da-458a-af84-ede3c4911c2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2dc9dc17-753f-4477-a3d3-7946f3a9c7b8"
        },
        {
            "id": "efd3c13b-2d2d-48d3-852d-c399f0350700",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2dc9dc17-753f-4477-a3d3-7946f3a9c7b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d89be49e-3120-4a21-89d1-9162e689d99a",
    "visible": true
}