{
    "id": "ce8801c0-655e-434b-ada8-da86a1510204",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_minussqempty",
    "eventList": [
        {
            "id": "974fd7b9-699a-4a0d-b956-0b0e419819c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "ce8801c0-655e-434b-ada8-da86a1510204"
        },
        {
            "id": "6bdfa119-bf3a-4c52-b86f-7d2ff612f65f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce8801c0-655e-434b-ada8-da86a1510204"
        },
        {
            "id": "631465d9-dc31-409d-829b-5658286427a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ce8801c0-655e-434b-ada8-da86a1510204"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "457f74c4-9f9f-4d52-897e-ab3a457011fe",
    "visible": true
}