{
    "id": "375cd045-7642-4757-8289-f9b23ecd00ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_trapswitch",
    "eventList": [
        {
            "id": "369c54d6-d741-494a-b840-65e12c5db599",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "375cd045-7642-4757-8289-f9b23ecd00ce"
        },
        {
            "id": "961623cb-9b47-4778-93f8-e9016054618a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "375cd045-7642-4757-8289-f9b23ecd00ce"
        },
        {
            "id": "72f5b761-b7ac-486a-a51d-04d6a7ce7e76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "375cd045-7642-4757-8289-f9b23ecd00ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1db5e248-e8cd-4dcc-97db-35a869a8cbb3",
    "visible": true
}