{
    "id": "0bf04c8a-58d6-4ae4-ad94-dd9faece606a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_gjflag",
    "eventList": [
        {
            "id": "ed6cc1ce-d9e0-4d78-b540-2593126db247",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0bf04c8a-58d6-4ae4-ad94-dd9faece606a"
        },
        {
            "id": "e5786aa5-e399-4e6a-a20c-e2e7aec03912",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0bf04c8a-58d6-4ae4-ad94-dd9faece606a"
        },
        {
            "id": "26104244-6992-445b-9670-c88cbd040fef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0bf04c8a-58d6-4ae4-ad94-dd9faece606a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d181aa31-9d93-4358-9892-c0f1af9d4114",
    "visible": true
}