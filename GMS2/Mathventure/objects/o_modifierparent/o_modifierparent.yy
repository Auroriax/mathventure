{
    "id": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_modifierparent",
    "eventList": [
        {
            "id": "98b8bbec-965b-4c55-b656-64568d29f4e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f0a7903-c4ff-48db-bf4b-2931b6327e07"
        },
        {
            "id": "f95685fd-c606-4d65-aca1-6d69309000ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1f0a7903-c4ff-48db-bf4b-2931b6327e07"
        },
        {
            "id": "ba44d548-8e5d-463a-902a-50100732296f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f0a7903-c4ff-48db-bf4b-2931b6327e07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}