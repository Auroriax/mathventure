{
    "id": "68a5f10c-f6d2-443f-8a45-e7a42fb658a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_greenwall",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "383e757d-003f-4290-a476-5b06c63a7a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "5b9b50ee-3195-4ea6-8f29-37ab251ece60",
    "visible": true
}