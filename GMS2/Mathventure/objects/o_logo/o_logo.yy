{
    "id": "3b1e27b6-bb57-4232-8ef1-1cbf54f534d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_logo",
    "eventList": [
        {
            "id": "8b9cd279-46bd-4943-8b17-a9e69d5aa04d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3b1e27b6-bb57-4232-8ef1-1cbf54f534d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bada8614-45aa-4966-a96d-330dca4898bb",
    "visible": true
}