{
    "id": "73460590-0c46-4b50-98cb-744bfa757f65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_levelclear",
    "eventList": [
        {
            "id": "62ab8f01-e41c-43f4-a894-084550a23b54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73460590-0c46-4b50-98cb-744bfa757f65"
        },
        {
            "id": "e7c1718e-be1a-40dd-90dc-3c4d7c86bda1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "73460590-0c46-4b50-98cb-744bfa757f65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3eecb3cf-362b-44a2-9e48-f6a16c88cbd3",
    "visible": true
}