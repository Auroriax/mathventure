{
    "id": "383e757d-003f-4290-a476-5b06c63a7a26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_wall",
    "eventList": [
        {
            "id": "f488272d-fbc9-42f7-ab62-a8f538920652",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "383e757d-003f-4290-a476-5b06c63a7a26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "0d9154a4-998f-4a90-bcb2-18cb6722ef3f",
    "visible": true
}