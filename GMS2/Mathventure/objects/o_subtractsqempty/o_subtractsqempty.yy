{
    "id": "cbd14667-eb77-4637-af52-b18a57181640",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_subtractsqempty",
    "eventList": [
        {
            "id": "d5a1c3e9-9e3e-4cf2-aa29-2b3f83c4a477",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cbd14667-eb77-4637-af52-b18a57181640"
        },
        {
            "id": "e1281bfa-36aa-44ac-b1f5-88c5e5baa6c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "12a8d8ed-6161-44a7-b3b8-76d7dff6c794",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cbd14667-eb77-4637-af52-b18a57181640"
        },
        {
            "id": "aed27b60-26be-45fe-95e9-253506624fa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cbd14667-eb77-4637-af52-b18a57181640"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1f0a7903-c4ff-48db-bf4b-2931b6327e07",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b06f6a47-68e1-4ed0-9b47-632981ecc633",
    "visible": true
}