{
    "id": "fb75e1e8-b48b-48d2-8540-3c140be22490",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tutflag",
    "eventList": [
        {
            "id": "663aa1fa-d164-4f6d-bfcc-0c019beec59d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb75e1e8-b48b-48d2-8540-3c140be22490"
        },
        {
            "id": "d779fa76-be89-4479-ad41-a97ce8b73237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fb75e1e8-b48b-48d2-8540-3c140be22490"
        },
        {
            "id": "f5dd6375-3539-46a4-b822-4ce532900210",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fb75e1e8-b48b-48d2-8540-3c140be22490"
        },
        {
            "id": "b9252ca6-01e9-4404-9e80-e980784d0732",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "fb75e1e8-b48b-48d2-8540-3c140be22490"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f508cd01-5212-4a0f-af71-f08c7d63c63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbe8bacc-8dd9-45f6-bb74-74b4331c7b95",
    "visible": true
}