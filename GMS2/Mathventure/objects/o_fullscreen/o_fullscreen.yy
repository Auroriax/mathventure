{
    "id": "ca8194b8-a836-418c-80d5-e1aeb5e4a147",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_fullscreen",
    "eventList": [
        {
            "id": "98d802fd-bec2-44b3-b755-bde9a8a69d82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca8194b8-a836-418c-80d5-e1aeb5e4a147"
        },
        {
            "id": "2b12533c-97e5-43a9-b22f-278026b039b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca8194b8-a836-418c-80d5-e1aeb5e4a147"
        },
        {
            "id": "a52de42c-4466-49c4-8f65-ada25c6cb8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ca8194b8-a836-418c-80d5-e1aeb5e4a147"
        },
        {
            "id": "954819b8-a87a-43b2-af11-99cc6002bc25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ca8194b8-a836-418c-80d5-e1aeb5e4a147"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aef14e87-e8a1-4bfe-8767-0ea273f5a95a",
    "visible": true
}