if os_browser == browser_ie
{
	showwarning("Audio is not supported in this browser. Please upgrade or change your browser.")
}
else if audio_system_is_available() == false
{
	showwarning("We don't have permission from your browser to play audio. Please check your preferences.")
}
else
{
	global.sfx = !global.sfx;

	if global.sfx
	{
	    audio(a_pickup);
	}
}
