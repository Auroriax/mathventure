{
    "id": "f933befb-be0e-4fe5-a8ce-413e20ce4e30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sfx",
    "eventList": [
        {
            "id": "fe2fa815-9056-40cd-98a4-7f80020ce0e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f933befb-be0e-4fe5-a8ce-413e20ce4e30"
        },
        {
            "id": "7de2c4f7-70f3-4170-9fdf-4a999fec883d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f933befb-be0e-4fe5-a8ce-413e20ce4e30"
        },
        {
            "id": "bb51edd7-820c-4444-90b5-a3f2e8834531",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f933befb-be0e-4fe5-a8ce-413e20ce4e30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "412446ab-1634-4500-98ba-ca19dc10f17f",
    "visible": true
}