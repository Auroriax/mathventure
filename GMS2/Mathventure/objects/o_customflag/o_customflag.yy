{
    "id": "228fbe0d-c5c2-4eb5-a03d-1ffb37f81a2e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_customflag",
    "eventList": [
        {
            "id": "ba99b661-2e5d-453d-b54e-640422f973e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "228fbe0d-c5c2-4eb5-a03d-1ffb37f81a2e"
        },
        {
            "id": "a1e5ffc5-a7ff-4292-8b2b-0fdb32c756f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "228fbe0d-c5c2-4eb5-a03d-1ffb37f81a2e"
        },
        {
            "id": "ab9e7e73-4f0e-4df2-a809-deddd5a2a6f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e3d91bff-2023-4863-abde-7c1d1e045436",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "228fbe0d-c5c2-4eb5-a03d-1ffb37f81a2e"
        },
        {
            "id": "fd70340b-daed-40b9-96d7-acae845223ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "228fbe0d-c5c2-4eb5-a03d-1ffb37f81a2e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f508cd01-5212-4a0f-af71-f08c7d63c63b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b843077b-9bd2-4e38-a409-2e7a3c188c47",
    "visible": true
}