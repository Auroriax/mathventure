{
    "id": "4e80dad9-513c-4a33-ae07-f50876eee694",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "coolmath",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 9223372036854775807,
    "date": "2019-02-14 05:11:10",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "b259d4c7-be32-420e-bb86-a8b1a15ab600",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "coolmath_functions.js",
            "final": "",
            "functions": [
                {
                    "id": "a54ff887-2d49-4664-bdda-b3abff94c738",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolmathCallStart",
                    "help": "Whenever a \"Play\" button is pressed.",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolmathCallStart",
                    "returnType": 2
                },
                {
                    "id": "4f4a33b1-c8d8-41d4-b54b-f6ef84c3f25f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolmathCallLevelStart",
                    "help": "Whenever a new level loads, passing the level number",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolmathCallLevelStart",
                    "returnType": 2
                },
                {
                    "id": "e658e64e-2146-4713-804f-114e941c02ec",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolmathCallLevelRestart",
                    "help": "Whenever the level is manually (not automatically) restarted, passing level number",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolmathCallLevelRestart",
                    "returnType": 2
                },
                {
                    "id": "bce8ddd4-1f38-4bdc-8c3d-adc4f56ad090",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "coolMathShouldUnlockAll",
                    "help": "Check regularily whether a subscriber wants to unlock all levels",
                    "hidden": false,
                    "kind": 11,
                    "name": "coolMathShouldUnlockAll",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                "a54ff887-2d49-4664-bdda-b3abff94c738",
                "4f4a33b1-c8d8-41d4-b54b-f6ef84c3f25f",
                "e658e64e-2146-4713-804f-114e941c02ec",
                "bce8ddd4-1f38-4bdc-8c3d-adc4f56ad090"
            ],
            "origname": "extensions\\coolmath_functions.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": 9223372036854775807,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.0"
}