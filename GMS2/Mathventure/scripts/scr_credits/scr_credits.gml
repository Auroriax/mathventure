/// @description scr_drawcredits(x,y,font1,text1,font2,text2,font3,text3,font4,text4,font5,text5,etc)
/// @param x
/// @param y
/// @param font1
/// @param text1
/// @param font2
/// @param text2
/// @param font3
/// @param text3
/// @param font4
/// @param text4
/// @param font5
/// @param text5
/// @param etc
//Returns how long the credits text is.

var sproffset = 100;

var yoff = argument[1];

var w = __view_get( e__VW.WView, 0 ) - 300

for(var i = 2; i < argument_count; i += 1)
{
    
    draw_text_outline(argument[0],yoff,argument[i],c_black,w)
    draw_text_ext(argument[0],yoff,string_hash_to_newline(argument[i]),-1,w)
    
    yoff += string_height_ext(string_hash_to_newline(argument[i]),-1,w) + 20
}
