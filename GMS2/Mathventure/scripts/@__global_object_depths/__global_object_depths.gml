// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = -1000000; // o_control
global.__objectDepths[1] = 0; // o_modifierparent
global.__objectDepths[2] = 100; // o_addroundempty
global.__objectDepths[3] = 100; // o_addsqempty
global.__objectDepths[4] = 100; // o_minusrdempty
global.__objectDepths[5] = 100; // o_minussqempty
global.__objectDepths[6] = 100; // o_multiplerdempty
global.__objectDepths[7] = 100; // o_multiplesqempty
global.__objectDepths[8] = 100; // o_subtractrdempty
global.__objectDepths[9] = 100; // o_subtractsqempty
global.__objectDepths[10] = 0; // o_hero
global.__objectDepths[11] = 0; // o_wall
global.__objectDepths[12] = 0; // o_wallsp
global.__objectDepths[13] = 0; // o_greenwall
global.__objectDepths[14] = 0; // o_greenwallsp
global.__objectDepths[15] = 0; // o_bluewall
global.__objectDepths[16] = 0; // o_bluewallsp
global.__objectDepths[17] = 99; // o_trapswitch
global.__objectDepths[18] = 0; // o_trapactive
global.__objectDepths[19] = 100; // o_flickleft
global.__objectDepths[20] = 100; // o_flickup
global.__objectDepths[21] = 100; // o_flickright
global.__objectDepths[22] = 100; // o_flickdown
global.__objectDepths[23] = 0; // o_flagparent
global.__objectDepths[24] = 0; // o_customflag
global.__objectDepths[25] = 0; // o_tutflag
global.__objectDepths[26] = 0; // o_importantflag
global.__objectDepths[27] = 0; // o_gjflag
global.__objectDepths[28] = 0; // o_secret
global.__objectDepths[29] = 1000; // o_goal
global.__objectDepths[30] = 0; // o_mapcontrol
global.__objectDepths[31] = -100; // o_tuttrigger
global.__objectDepths[32] = -1000000; // o_dot
global.__objectDepths[33] = 0; // o_cloudparent
global.__objectDepths[34] = -1000; // o_cloud1
global.__objectDepths[35] = -900; // o_cloud2
global.__objectDepths[36] = -800; // o_cloud3
global.__objectDepths[37] = -700; // o_cloud4
global.__objectDepths[38] = -10000; // o_logo
global.__objectDepths[39] = 0; // o_credits
global.__objectDepths[40] = 0; // o_completion
global.__objectDepths[41] = -1000000; // o_levelclear
global.__objectDepths[42] = -1; // o_framework
global.__objectDepths[43] = 0; // o_achievementchecker
global.__objectDepths[44] = 0; // o_unrelevant
global.__objectDepths[45] = 0; // o_trapbronze
global.__objectDepths[46] = 0; // o_trapsilver
global.__objectDepths[47] = 0; // o_loggedin
global.__objectDepths[48] = -10001; // o_mute
global.__objectDepths[49] = -10001; // o_sfx
global.__objectDepths[50] = -1000; // o_fullscreen
global.__objectDepths[51] = 0; // o_flagpulsate
global.__objectDepths[52] = 0; // o_coolMathSplash
global.__objectDepths[53] = 0; // o_coolmath


global.__objectNames[0] = "o_control";
global.__objectNames[1] = "o_modifierparent";
global.__objectNames[2] = "o_addroundempty";
global.__objectNames[3] = "o_addsqempty";
global.__objectNames[4] = "o_minusrdempty";
global.__objectNames[5] = "o_minussqempty";
global.__objectNames[6] = "o_multiplerdempty";
global.__objectNames[7] = "o_multiplesqempty";
global.__objectNames[8] = "o_subtractrdempty";
global.__objectNames[9] = "o_subtractsqempty";
global.__objectNames[10] = "o_hero";
global.__objectNames[11] = "o_wall";
global.__objectNames[12] = "o_wallsp";
global.__objectNames[13] = "o_greenwall";
global.__objectNames[14] = "o_greenwallsp";
global.__objectNames[15] = "o_bluewall";
global.__objectNames[16] = "o_bluewallsp";
global.__objectNames[17] = "o_trapswitch";
global.__objectNames[18] = "o_trapactive";
global.__objectNames[19] = "o_flickleft";
global.__objectNames[20] = "o_flickup";
global.__objectNames[21] = "o_flickright";
global.__objectNames[22] = "o_flickdown";
global.__objectNames[23] = "o_flagparent";
global.__objectNames[24] = "o_customflag";
global.__objectNames[25] = "o_tutflag";
global.__objectNames[26] = "o_importantflag";
global.__objectNames[27] = "o_gjflag";
global.__objectNames[28] = "o_secret";
global.__objectNames[29] = "o_goal";
global.__objectNames[30] = "o_mapcontrol";
global.__objectNames[31] = "o_tuttrigger";
global.__objectNames[32] = "o_dot";
global.__objectNames[33] = "o_cloudparent";
global.__objectNames[34] = "o_cloud1";
global.__objectNames[35] = "o_cloud2";
global.__objectNames[36] = "o_cloud3";
global.__objectNames[37] = "o_cloud4";
global.__objectNames[38] = "o_logo";
global.__objectNames[39] = "o_credits";
global.__objectNames[40] = "o_completion";
global.__objectNames[41] = "o_levelclear";
global.__objectNames[42] = "o_framework";
global.__objectNames[43] = "o_achievementchecker";
global.__objectNames[44] = "o_unrelevant";
global.__objectNames[45] = "o_trapbronze";
global.__objectNames[46] = "o_trapsilver";
global.__objectNames[47] = "o_loggedin";
global.__objectNames[48] = "o_mute";
global.__objectNames[49] = "o_sfx";
global.__objectNames[50] = "o_fullscreen";
global.__objectNames[51] = "o_flagpulsate";
global.__objectNames[52] = "o_coolMathSplash";
global.__objectNames[53] = "o_coolmath";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for