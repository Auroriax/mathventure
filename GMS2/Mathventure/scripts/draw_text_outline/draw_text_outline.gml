/// @description  draw_text_outline(x,y,string[,outercolor, width, alpha, separation, multiplier,rotation])
/// @param x
/// @param y
/// @param string[
/// @param outercolor
/// @param  width
/// @param  alpha
/// @param  separation
/// @param  multiplier
/// @param rotation]
// Make sure that when you call this function, you call a normal draw_text with the same position and string, 
// but a different color immediatly AFTER this function!

var clr = draw_get_colour();
var wdt = -1; var sep = -1; var alph = draw_get_alpha(); var mpl = 1; var xsc = 1; var ysc = 1; var rot = 0;
if argument_count >= 4 {clr = argument[3]} //Set draw color
if argument_count >= 5 {wdt = argument[4]} //Set text maximum width
if argument_count >= 6 {alph = argument[5]} //Set alpha of outline (usually recommended to keep this at 1)
if argument_count >= 7 {sep = argument[6]} //Text seperation value
if argument_count >= 8 {mpl = argument[7]} //If you're drawing a big font, if might be worth to make this higher so you have a thicker border
if argument_count >= 9 {rot = argument[8]} //Text rotation
draw_text_ext_transformed_color(argument[0]+mpl,argument[1] +mpl,string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
draw_text_ext_transformed_color(argument[0]+mpl,argument[1] -mpl,string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
draw_text_ext_transformed_color(argument[0]-mpl,argument[1] +mpl,string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
draw_text_ext_transformed_color(argument[0]-mpl,argument[1] -mpl,string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);

draw_text_ext_transformed_color(argument[0],argument[1] -mpl,string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
draw_text_ext_transformed_color(argument[0] -mpl-1,argument[1],string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
draw_text_ext_transformed_color(argument[0],argument[1] +mpl,string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
draw_text_ext_transformed_color(argument[0] +mpl+1,argument[1],string_hash_to_newline(argument[2]),sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph);
