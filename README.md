# Mathventure by @Auroriax [(auroriax.com)](http://auroriax.com) #

![ed8adc3416fe97dc34b76e65e7aba7ba.png](https://bitbucket.org/repo/njaEeA/images/2457798800-ed8adc3416fe97dc34b76e65e7aba7ba.png)

Mathventure is a puzzle game about calculating your way through mazes and use the various operators on the field to add, decrease, multiply or divide your score to match the number on the goal. It was originally made in 2013 in Game Maker 8, then ported to HTML5 in 2016. [You can play it here.](https://auroriax.itch.io/mathventure)

**The project will no longer be actively maintained, and the source code is primarily available for documentation & archiving purposes. You can open this project with Game Maker Studio 1.4, [which you can obtain here.](http://www.yoyogames.com/get)**

## Licence ##
For the rights of the music and fonts, [see the credits of the game](https://bitbucket.org/Auroriax/mathventure/src/7170ab802341a17655dceb003209f454d07d9951/sprites/images/s_credits_0.png?at=master&fileviewer=file-view-default). This licence applies to everything else in the repository. 

Copyright (c) 2013-2017, Tom 'Auroriax' Hermans

Permission is hereby granted, free of charge, to any person (the "Person") obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software, including the rights to use, copy, modify, merge, publish, distribute the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Under no circumstances shall the Person be permitted, allowed or authorized to commercially exploit the Software.

3. Changes made to the original Software shall be labeled, demarcated or otherwise identified and attributed to the Person.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.